<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('main.index');
//})->name('main.page');

Route::get('/', [\App\Http\Controllers\Main\MainController::class, 'index'])->name('main.page');
Route::get('/category/{id}',[\App\Http\Controllers\Main\MainController::class, 'category_filter'])->name('category.filter');
Route::get('/subcategory/{id}',[\App\Http\Controllers\Main\MainController::class, 'subcategory_filter'])->name('subcategory.filter');
Route::get('/price/{prc}', [\App\Http\Controllers\Main\MainController::class, 'price_filter'])->name('price.filter');
Route::get('/size/{sz}', [\App\Http\Controllers\Main\MainController::class, 'size_filter'])->name('size.filter');
Route::get('/type/{type}', [\App\Http\Controllers\Main\MainController::class, 'type_filter'])->name('type.filter');


Route::middleware(['middleware'=>'back'])->group(function (){
    Auth::routes();
});





//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix'=>'dashboard', 'middleware'=>['auth','admin','back']], function (){
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'adminpanel'])->name('adminpanel');
    Route::get('/setting', [\App\Http\Controllers\HomeController::class, 'edit'])->name('dashboard.setting');
    Route::put('/update/{user}',[\App\Http\Controllers\HomeController::class, 'update'])->name('dashboard.update');
    Route::resource('/role', \App\Http\Controllers\Admin\RoleController::class);
    Route::get('/notifications', [\App\Http\Controllers\User\PetitionController::class, 'notifications'])->name('dashboard.notifications');
    Route::get('/notification/{petition}', [\App\Http\Controllers\User\PetitionController::class, 'notificationshow'])->name('dashboard.notificationshow');
    Route::get('/notification/accept/{petition}',[\App\Http\Controllers\User\PetitionController::class, 'notification_accept'])->name('dashboard.notification_accept');
    Route::post('/notification/cancel/{petition}',[\App\Http\Controllers\User\PetitionController::class, 'notification_cancel'])->name('dashboard.notification_cancel');
    Route::resource('/category', \App\Http\Controllers\Admin\CategoryController::class);
    Route::resource('/subcategory', \App\Http\Controllers\Admin\SubcategoryController::class);
    Route::get('/architectors', [\App\Http\Controllers\Admin\MembersController::class, 'architectors'])->name('dashboard.architectors');
    Route::get('/users', [\App\Http\Controllers\Admin\MembersController::class, 'users'])->name('dashboard.users');
    Route::get('/receive', [\App\Http\Controllers\Architector\ProductController::class, 'recieve'])->name('product.recieve');
    Route::get('/receive/{product}', [\App\Http\Controllers\Architector\ProductController::class, 'show_receive'])->name('product.show_receive');
    Route::post('/product/block', [\App\Http\Controllers\Architector\ProductController::class, 'accept_block'])->name('product.block');
    Route::get('/product/accepts', [\App\Http\Controllers\Architector\ProductController::class, 'accept_models'])->name('product.accept');
    Route::get('/product/cancels', [\App\Http\Controllers\Architector\ProductController::class, 'cancel_models'])->name('product.cancel');
    Route::get('/product/press_accept/{product}', [\App\Http\Controllers\Architector\ProductController::class, 'press_accept'])->name('products.press_accept');
    Route::post('/product/cancel/{product}', [\App\Http\Controllers\Architector\ProductController::class, 'reason'])->name('product.reason');
    Route::post('/spam/architector/{architector}', [\App\Http\Controllers\Architector\SpamController::class, 'architec_spam'])->name('architec.spam');
    Route::get('/nospam/architector/{architector}', [\App\Http\Controllers\Architector\SpamController::class, 'architec_nospam'])->name('architec.nospam');
    Route::post('/spam/user/{user}', [\App\Http\Controllers\Architector\SpamController::class, 'user_spam'])->name('user.spam');
    Route::get('/nospam/user/{user}', [\App\Http\Controllers\Architector\SpamController::class, 'user_nospam'])->name('user.nospam');
    Route::get('/messenger', [\App\Http\Controllers\Admin\AmessengerController::class, 'create'])->name('members.messenger');
    Route::get('/chat/{member}', [\App\Http\Controllers\Admin\AmessengerController::class, 'chat_member'])->name('members.chat');
    Route::post('/messege/text/{account}', [\App\Http\Controllers\Admin\AmessengerController::class, 'admin_message_text'])->name('admin_text_message');
    Route::post('/search', [\App\Http\Controllers\Admin\AmessengerController::class, 'search'])->name('members.search');
    Route::get('/contact', [\App\Http\Controllers\Main\ContactController::class, 'recieve_contact'])->name('recieve.contact');
});

Route::group(['prefix'=>'architector', 'middleware'=>['auth','architector','back']], function (){
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'archtecpanel'])->name('architecpanel');
    Route::get('/setting', [\App\Http\Controllers\HomeController::class, 'edit_arch'])->name('architec.setting');
    Route::put('/update/{user}', [\App\Http\Controllers\HomeController::class, 'update_arch'])->name('architec.update');
    Route::resource('/product', \App\Http\Controllers\Architector\ProductController::class);
    Route::post('/select/category', [\App\Http\Controllers\Architector\ProductController::class, 'select_category'])->name('architec.category');
    Route::post('/select/subcategory', [\App\Http\Controllers\Architector\ProductController::class,'select_subcategory'])->name('architec.subcategory');
    Route::get('/chat', [\App\Http\Controllers\Admin\AmessengerController::class, 'chat_with_admin'])->name('admin.chat');
    Route::post('/messege/text', [\App\Http\Controllers\Admin\AmessengerController::class, 'architec_text_message'])->name('architec_text_message');
});

Route::group(['prefix'=>'user', 'middleware'=>['auth','user','back']], function (){
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'userpanel'])->name('userpanel');
    Route::get('/setting', [\App\Http\Controllers\HomeController::class, 'edit_user'])->name('user.setting');
    Route::put('/update/{user}', [\App\Http\Controllers\HomeController::class, 'update_user'])->name('user.update');
    Route::resource('/petition', \App\Http\Controllers\User\PetitionController::class);
    Route::get('/chat', [\App\Http\Controllers\Admin\AmessengerController::class, 'user_chat_with_admin'])->name('user.chat');
    Route::post('/messege/text', [\App\Http\Controllers\Admin\AmessengerController::class, 'user_text_message'])->name('user_text_message');
});

Route::get('/contact', [\App\Http\Controllers\Main\ContactController::class, 'contact_page'])->name('main.contact');
Route::post('/send', [\App\Http\Controllers\Main\ContactController::class, 'store'])->name('contact.store');
Route::get('/product/detail/{product_accept}', [\App\Http\Controllers\Architector\ProductController::class, 'product_detail'])->name('product_detail');
