@extends('architector.layouts.master')
@section('title','View your products')
@section('content')

<br><br>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5>Products table</h5>
            <a href="{{route('product.create')}}" class="btn btn-primary">create product</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="display" id="basic-1">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Model name</th>
                        <th>Category</th>
                        <th>Model price</th>
                        <th>Status</th>
                        <th>Status reason</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $key => $product)
                        <tr>
                            <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                <strong> {{$key+1}} </strong>
                            </td>
                            <td>
                                {{$product->model_name}}
                            </td>
                            <td>
                                {{$product->category->category_name}}
                            </td>
                            <td>
                                {{$product->model_price}}&nbsp;sum
                            </td>
                            <td>
                                @if($product->status==0)
                                    <span class="badge badge-warning">checking...</span>
                                @elseif($product->status==1)
                                    <span class="badge badge-success">accept</span>
                                @elseif($product->status==2)
                                    <span class="badge badge-danger">cancel</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-dark" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalfatCancel{{$key}}" data-whatever="@mdo" data-bs-original-title="" title="">Reason</button>
                                <div class="modal fade" id="exampleModalfatCancel{{$key}}" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel2">Send <span class="badge badge-danger">cancel</span> description to user</h5>
                                                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
                                            </div>
                                                <div class="modal-body">

                                                    <div class="mb-3">
                                                        <label class="col-form-label" for="message-text">Message:</label>
                                                        <p>{{$product->status_reason}}</p>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                                </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                            <td class="d-flex">
                                <a href="{{route('product.show', $product)}}" class="btn btn-secondary">Detail</a>&nbsp;
                                <a href="{{route('product.edit', $product)}}" class="btn btn-primary">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete petition?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>

@endpush
