@extends('architector.layouts.master')
@section('title','Update product')
@section('content')
    <br><br>
    <div class="col-md-12">
        <div class="card mb-4">
            <form action="{{route('product.update', $product)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <h5 class="card-header">Update product 3D model</h5>
                <div class="card-body">
                    <div>
                        <label for="defaultFormControlInput" class="form-label">Model name</label>
                        <input value="{{$product->model_name}}" name="model_name" type="text" class="form-control @error('model_name') is-invalid @enderror" id="defaultFormControlInput" placeholder="Model name" aria-describedby="defaultFormControlHelp">
                        @error('model_name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <br>
                    <div class="mb-3">
                        <label for="exampleFormControlSelect1" class="form-label">Select category</label>
                        <select name="category_id" class="form-select @error('category_id') is-invalid @enderror" id="category_id" aria-label="Default select example">
{{--                            <option selected disabled>select category</option>--}}
                            @foreach($categories as $category)
                                @if($product->category_id == $category->id)
                                    <option selected value="{{$category->id}}">{{$category->category_name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('category_id')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <br>
                    <div class="mb-3">
                        <label for="exampleFormControlSelect1" class="form-label">Select subcategory</label>
                        <select name="subcategory_id" class="form-select @error('subcategory_id') is-invalid @enderror" id="subcategory_id" aria-label="Default select example">
                            @foreach($subcategories as $subcategory)
                                @if($product->subcategory_id == $subcategory->id)
                                    <option selected value="{{$subcategory->id}}">{{$subcategory->subcategory_name}}</option>
                                @elseif($product->category_id == $subcategory->category_id)
                                    <option value="{{$subcategory->id}}">{{$subcategory->subcategory_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('subcategory_id')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div>
                        <label for="defaultFormControlInput" class="form-label">Model price (so'm)</label>
                        <input value="{{$product->model_price}}"  name="model_price" type="number" class="form-control @error('model_price') is-invalid @enderror" id="defaultFormControlInput" placeholder="Price" aria-describedby="defaultFormControlHelp">
                        @error('model_price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <br>
                    <div>
                        <label for="defaultFormControlInput" class="form-label">Model images</label>
                        <input multiple name="model_img[]" type="file" class="form-control @error('model_img') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        @error('model_img')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @foreach ($errors->get('model_img.*') as $error)
                            <div class="alert alert-danger">{{ $error[0] }}</div>
                        @endforeach
                    </div>
                    <br>
                    @for($i=0; $i<count($model_small_images)-1; $i++)
                        <img width="180" height="90" src="{{asset('architec/models/images/small/'.$model_small_images[$i])}}" alt="">
                    @endfor
                    <br>
                    <br>
                    <div>
                        <label for="defaultFormControlInput" class="form-label">Model file (.max, .blend, .obj, .fbx, .revit)</label>
                        <input type="file" name="model_file" class="form-control @error('model_file') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        @error('model_file')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <a class="text-danger" href="{{asset('architec/models/files/'.$product->model_type.'/'.$product->model_file)}}">{{$product->model_name}} 3D model</a>
                    <br>
                    <br>
                    <div>
                        <label for="exampleFormControlTextarea1" class="form-label">Model description</label>
                        <textarea class="form-control @error('model_description') is-invalid @enderror" name="model_description" id="exampleFormControlTextarea1" rows="10">{{$product->model_description}}</textarea>
                        @error('model_description')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <br>
                    <div>
                        <input type="submit" class="btn btn-dark" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp" value="update">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#category_id").change(function(){
                $("#subcategory_id").html('');
                var category_id = $(this).val();
                $("#subcategory_id").append('<option value="null" selected disabled>select subcategory</option>');
                $.ajax({
                    url:'{{route('architec.category')}}',
                    method: 'POST',
                    data:{
                        'category_id' : category_id
                    },
                    success:function (response) {

                        for (i=0; i<response.subcategories.length; i++)
                        {
                            $("#subcategory_id").append('<option value='+response.subcategories[i].id+'>'+response.subcategories[i].subcategory_name+'</option>');
                        }
                    },
                    error:function (response) {
                        console.log(response)
                    }
                });
            });


            $("#subcategory_id").change(function(){
                $("#category_id").html('');
                var subcategory_id = $(this).val();
                $.ajax({
                    url:'{{route('architec.subcategory')}}',
                    method: 'POST',
                    data:{
                        'subcategory_id' : subcategory_id
                    },
                    success:function (response) {

                        // console.log(response.categories);
                        console.log(response.category_id.category_id);
                        for (i=0; i<response.categories.length; i++)
                        {
                            if(response.category_id.category_id === response.categories[i].id)
                            {
                                $("#category_id").append('<option selected value='+response.categories[i].id+'>'+response.categories[i].category_name+'</option>');
                            }else {
                                $("#category_id").append('<option value='+response.categories[i].id+'>'+response.categories[i].category_name+'</option>');
                            }
                        }
                    },
                    error:function (response) {
                        console.log(response)
                    }
                });
            });

        });

    </script>

@endpush
{{--@dump($errors)--}}
