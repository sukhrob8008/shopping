@extends('architector.layouts.master')
@section('title','Messanger')
@section('content')
    <br>
    <div class="row">
        <div class="col call-chat-sidebar col-sm-12">
            <div class="card">
                <div class="card-body chat-body">
                    <div class="chat-box">
                        <!-- Chat left side Start-->
                        <div class="chat-left-aside">
                            <div class="media"><img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                <div class="about">
                                    <div class="name f-w-600">{{\Illuminate\Support\Facades\Auth::user()->name.' '.\Illuminate\Support\Facades\Auth::user()->lname}}</div>
                                    <div class="status">Status...</div>
                                </div>
                            </div>
                            <div class="people-list" id="people-list">
                                <div class="search">
                                    <div class="mb-3">
                                        <input id="search" class="form-control" type="text" placeholder="Search" data-bs-original-title="" title=""><i class="fa fa-search"></i>
                                    </div>
                                </div>
                                <ul class="list" id="members_list">
                                        <li class="clearfix"><img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/5.jpg')}}" alt="">
                                            <a href="">
                                                <div class="status-circle away"></div>
                                                <div class="about">
                                                    <div class="name">Administrator</div>
                                                    <div class="status">
                                                       Admin
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Chat left side Ends-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col call-chat-body">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row chat-box">
                        <!-- Chat right side start-->
                        <div class="col pe-0 chat-right-aside">
                            <!-- chat start-->
                            <div class="chat">
                                <!-- chat-header start-->
                                <div class="chat-header clearfix"><img class="rounded-circle" src="{{asset('admin/assets/images/user/8.jpg')}}" alt="">
                                    <div class="about">
                                        <div class="name">Administrator&nbsp;&nbsp;<span class="font-primary f-12">Typing...</span></div>
                                        <div class="status">Last Seen 3:55 PM</div>
                                    </div>
                                    <ul class="list-inline float-start float-sm-end chat-menu-icons">
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-search"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-clip"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-headphone-alt"></i></a></li>
                                        <li class="list-inline-item"><a href="#" data-bs-original-title="" title=""><i class="icon-video-camera"></i></a></li>
                                        <li class="list-inline-item toogle-bar"><a href="#" data-bs-original-title="" title=""><i class="icon-menu"></i></a></li>
                                    </ul>
                                </div>
                                <!-- chat-header end-->
                                <div class="chat-history chat-msg-box custom-scrollbar">
                                    <ul>
                                        @foreach($old_messeges_with_admin as $old_messege_with_admin)
                                            @if($old_messege_with_admin->admin_messege == null)
                                                <li class="clearfix">
                                                    <div class="message other-message pull-right"><img class="rounded-circle float-end chat-user-img img-30" src="../assets/images/user/12.png" alt="">
                                                        {{$old_messege_with_admin->architec_messege}}
                                                        <div class="message-data"><span class="message-data-time">{{$old_messege_with_admin->created_at}}</span></div>

                                                    </div>
                                                </li>

                                            @elseif($old_messege_with_admin->architec_messege == null)
                                                <li>
                                                    <div class="message my-message"><img class="rounded-circle float-start chat-user-img img-30" src="../assets/images/user/3.png" alt="">
                                                        {{$old_messege_with_admin->admin_messege}}
                                                        <div class="message-data text-end"><span class="message-data-time">{{$old_messege_with_admin->created_at}}</span></div>
                                                    </div>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- end chat-history-->
                                <div class="chat-message clearfix">
                                    <div class="row">
                                        <form action="{{route('architec_text_message')}}" method="post">
                                            @csrf
                                            <div class="col-xl-12 d-flex">
                                                <div class="input-group text-box">
                                                    <input autocomplete="off" class="form-control @error('architec_messege') is-invalid @enderror" id="message-to-send" type="text" name="architec_messege" placeholder="Type a message......" data-bs-original-title="" title="">
                                                    <button class="btn btn-primary" type="submit" data-bs-original-title="" title="">send</button>
                                                </div>
                                                @error('architec_messege')
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                @enderror
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
