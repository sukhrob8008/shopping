@extends('architector.layouts.master')
@section('title', 'Architec edit page')
@section('content')
    <br>
    <div class="col-md-12">
        <div class="card mb-4">
            <form action="{{route('architec.update', \Illuminate\Support\Facades\Auth::user()->id)}}" method="post">
                @csrf
                @method('PUT')
                <h5 class="card-header">Update user info</h5>
                <div class="card-body">
                    <div>
                        <label for="defaultFormControlInput" class="form-label">Name</label>
                        <input value="{{\Illuminate\Support\Facades\Auth::user()->name}}" name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                            </span>
                        @enderror


                        <label for="defaultFormControlInput" class="form-label">Last Name</label>
                        <input value="{{\Illuminate\Support\Facades\Auth::user()->lname}}" name="lname" type="text" class="form-control @error('lname') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        @error('lname')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror


                        <label for="defaultFormControlInput" class="form-label">Email</label>
                        <input value="{{\Illuminate\Support\Facades\Auth::user()->email}}" name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror


                        <label for="defaultFormControlInput" class="form-label">Old password</label>
                        <input name="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        @error('old_password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror


                        <label for="defaultFormControlInput" class="form-label">Password</label>
                        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="defaultFormControlInput" class="form-label">Password confirm</label>
                        <input name="password_confirmation" type="password" class="form-control" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                        <br>
                        <input value="Update" type="submit" class="btn btn-success" id="defaultFormControlInput" aria-describedby="defaultFormControlHelp">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
