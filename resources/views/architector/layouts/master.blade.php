<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba architec is super flexible, powerful, clean &amp; modern responsive bootstrap 5 architec template with unlimited possibilities.">
    <meta name="keywords" content="architec template, Cuba architec template, dashboard template, flat architec template, responsive architec template, web app">
    <meta name="author" content="pixelstrap">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('architec/assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('architec/assets/images/favicon.png')}}" type="image/x-icon">
    <title>@yield('title')</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/font-awesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/feather-icon.css')}}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/date-picker.css')}}">

    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/owlcarousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/prism.css')}}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('architec/assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('architec/assets/css/vendors/select2.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body>
<!-- tap on top starts-->
<div class="tap-top"><i data-feather="chevrons-up"></i></div>
<!-- tap on tap ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper compact-wrapper" id="pageWrapper">
    <!-- Page Header Start-->
@include('architector.partials.navbar')
<!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
    @include('architector.partials.sidebar')
    <!-- Page Sidebar Ends-->
        <div class="page-body">
            @yield('content')
        </div>

    </div>
</div>
<!-- latest jquery-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{asset('architec/assets/js/jquery-3.5.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('architec/assets/js/bootstrap/bootstrap.bundle.min.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('architec/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('architec/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- scrollbar js-->
<script src="{{asset('architec/assets/js/scrollbar/simplebar.js')}}"></script>
<script src="{{asset('architec/assets/js/scrollbar/custom.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('architec/assets/js/config.js')}}"></script>
<!-- Plugins JS start-->
<script src="{{asset('architec/assets/js/select2/select2.full.min.js')}}"></script>
<script src="{{asset('architec/assets/js/select2/select2-custom.js')}}"></script>
<script src="{{asset('architec/assets/js/sidebar-menu.js')}}"></script>
<script src="{{asset('architec/assets/js/chart/chartist/chartist.js')}}"></script>
<script src="{{asset('architec/assets/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
<script src="{{asset('architec/assets/js/chart/knob/knob.min.js')}}"></script>
<script src="{{asset('architec/assets/js/chart/knob/knob-chart.js')}}"></script>
<script src="{{asset('architec/assets/js/chart/apex-chart/apex-chart.js')}}"></script>
<script src="{{asset('architec/assets/js/chart/apex-chart/stock-prices.js')}}"></script>
<script src="{{asset('architec/assets/js/notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('architec/assets/js/dashboard/default.js')}}"></script>
<script src="{{asset('architec/assets/js/notify/index.js')}}"></script>
<script src="{{asset('architec/assets/js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{asset('architec/assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{asset('architec/assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
<script src="{{asset('architec/assets/js/typeahead/handlebars.js')}}"></script>
<script src="{{asset('architec/assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('architec/assets/js/datatable/datatables/datatable.custom.js')}}"></script>
<script src="{{asset('architec/assets/js/typeahead/typeahead.bundle.js')}}"></script>
<script src="{{asset('architec/assets/js/typeahead/typeahead.custom.js')}}"></script>
<script src="{{asset('architec/assets/js/typeahead-search/handlebars.js')}}"></script>
<script src="{{asset('architec/assets/js/typeahead-search/typeahead-custom.js')}}"></script>


<script src="{{asset('architec/assets/js/prism/prism.min.js')}}"></script>
<script src="{{asset('architec/assets/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('architec/assets/js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('architec/assets/js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{asset('architec/assets/js/counter/counter-custom.js')}}"></script>
<script src="{{asset('architec/assets/js/custom-card/custom-card.js')}}"></script>
<script src="{{asset('architec/assets/js/owlcarousel/owl.carousel.js')}}"></script>
<script src="{{asset('architec/assets/js/dashboard/dashboard_2.js')}}"></script>
<script src="{{asset('architec/assets/js/tooltip-init.js')}}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('architec/assets/js/script.js')}}"></script>
<script src="{{asset('architec/assets/js/theme-customizer/customizer.js')}}"></script>
@include('sweetalert::alert')
@stack('script')
<!-- login js-->
<!-- Plugin used-->
</body>
</html>
