
<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper"><a href="index.html"><img class="img-fluid for-light" src="{{asset('architec/assets/images/logo/logo.png')}}" alt=""><img class="img-fluid for-dark" src="{{asset('architec/assets/images/logo/logo_dark.png')}}" alt=""></a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('architec/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="index.html"><img class="img-fluid" src="{{asset('architec/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6 class="lan-1">Moduls</h6>
                            <p class="lan-2">Dashboards,widgets & layout.</p>
                        </div>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="file-text"></i><span class="">3D models</span></a>
                        <ul class="sidebar-submenu {{(request()->is('architector/product/create') ||  request()->is('architector/product') ||  request()->is('architector/product/*/edit') ||  request()->is('architector/product/*')) ? 'd-block' : ''}}">
                            <li><a class="" style="color: {{request()->is('architector/product/create') ? '#6600FF' : ''}}" href="{{route('product.create')}}">Create 3D model</a></li>
                            <li><a class="" style="color: {{(request()->is('architector/product') ||  request()->is('architector/product/*/edit') ||  request()->routeIs('product.show')) ? '#6600FF' : ''}}" href="{{route('product.index')}}">View 3D models</a></li>
                        </ul>
                    </li>



                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
