@extends('user.layouts.master')
@section('title', 'Personal page')
@section('content')
    <br>
    <div class="col-lg-12 mb-4 order-0">
        <div class="card">
            <div class="d-flex align-items-end row">
                <div class="col-sm-12">
                    <div class="card-body">
                        <h5 class="card-title text-primary">Assalomu alaykum {{\Illuminate\Support\Facades\Auth::user()->name.' '.\Illuminate\Support\Facades\Auth::user()->lname}}! 🎉</h5>
                        <p class="mb-4 fw-bold">
                            Bizning 3D modellar onlayn do'konimizning platformasidan ro'yxatdan o'tganingiz uchun rahmat!,
                            Sizda sotilishi mumkin bo'lgan 3D modellar bo'lsa ularni sotishingiz uchun
                            Arizalar bo'limi orqali administratorga ariza qoldiring va arizangiz ko'rib chiqilgach sizga sotuvchi maqomi beriladi.
                        </p>

                        <a href="javascript:;" class="btn btn-sm btn-outline-primary">View Badges</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
