@extends('user.layouts.master')
@section('title','Create petition')
@section('content')
{{--    <div class="col-md-12">--}}
{{--        <div class="card mb-4">--}}
{{--            <form action="{{route('petition.store')}}" method="POST">--}}
{{--                @csrf--}}
{{--                @method('POST')--}}
{{--                <h5 class="card-header">You can send petition to Administrator for add to role Architector</h5>--}}
{{--                <div class="card-body">--}}
{{--                    <div>--}}
{{--                        <label for="exampleFormControlTextarea1" class="form-label">Petition text</label>--}}
{{--                        <textarea name="petition_text" class="form-control @error('petition_text') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>--}}
{{--                        @error('petition_text')--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <br>--}}
{{--                    <div>--}}
{{--                        <label for="exampleFormControlTextarea1" class="form-label">Your portfolio link</label>--}}
{{--                        <input placeholder="https://" type="text" class="form-control" name="portfolio_link">--}}
{{--                    </div>--}}
{{--                    <br>--}}
{{--                    <div>--}}
{{--                        <input type="submit" value="send" class="btn btn-success">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
<div class="select2-drpdwn">
    <div class="row">
        <div class="col-md-12">
            <div class="card mt-3">
                <form action="{{route('petition.store')}}" method="post">
                    @csrf
                    <div class="card-header">
                        <h5 class="card-title">Create petition</h5>
                    </div>
                    <div class="card-body o-hidden">
                        <div class="mb-2">
                            <div class="col-form-label">Petition text</div>
                            <textarea name="petition_text" class="form-control @error('petition_text') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                            @error('petition_text')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <div class="col-form-label">Portfolio link</div>
                            <input placeholder="https://..." name="portfolio_link" class="form-control @error('portfolio_link') is-invalid @enderror" id="exampleFormControlTextarea1"/>
                            @error('portfolio_link')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <input type="submit" class="btn btn-primary" value="send petition">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
