@extends('user.layouts.master')
@section('title','View your petitions')
@section('content')
{{--    <div class="card">--}}
{{--        <h5 class="card-header">Dark Table head</h5>--}}
{{--        <div class="table-responsive text-nowrap">--}}
{{--            <table class="table">--}}
{{--                <thead class="table-dark">--}}
{{--                <tr>--}}
{{--                    <th>#</th>--}}
{{--                    <th>Status</th>--}}
{{--                    <th>Petition text</th>--}}
{{--                    <th>Portfolio link</th>--}}
{{--                    <th>Result</th>--}}
{{--                    <th>Actions</th>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody class="table-border-bottom-0">--}}
{{--                @foreach($petitions as $key => $petition)--}}
{{--                <tr>--}}
{{--                    <td><i class="fab fa-angular fa-lg text-danger me-3"></i>--}}
{{--                        <strong> {{$key+1}} </strong>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        @if($petition->status == 0)--}}
{{--                            <span class="badge bg-label-warning me-1">Checking</span>--}}
{{--                        @elseif($petition->status == 1)--}}
{{--                            <span class="badge bg-label-success me-1">Accept</span>--}}
{{--                        @elseif($petition->status == 2)--}}
{{--                            <span class="badge bg-label-danger me-1">Cancel</span>--}}
{{--                        @endif--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        {{substr($petition->petition_text, 0,30)}}..--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        {{substr($petition->portfolio_link, 0, 30)}}..--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                            <div class="">--}}
{{--                                <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#basicModal">--}}
{{--                                    result--}}
{{--                                </button>--}}

{{--                                <!-- Modal -->--}}
{{--                                <div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true">--}}
{{--                                    <div class="modal-dialog" role="document">--}}
{{--                                        <div class="modal-content">--}}
{{--                                            <div class="modal-header">--}}
{{--                                                <h5 class="modal-title" id="exampleModalLabel1">Message from Administrator</h5>--}}
{{--                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>--}}
{{--                                            </div>--}}
{{--                                            <div class="modal-body">--}}
{{--                                                <div class="row">--}}
{{--                                                    <div class="col mb-3">--}}
{{--                                                        <p class="">{{$petition->result_description}}</p>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                            <div class="modal-footer">--}}
{{--                                                <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">--}}
{{--                                                    Close--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                    </td>--}}
{{--                    <td class="d-flex">--}}
{{--                        <a href="{{route('petition.edit', $petition)}}" class="btn btn-primary">Edit</a>--}}
{{--                        &nbsp;<form action="{{route('petition.destroy', $petition)}}" method="post">--}}
{{--                            @csrf--}}
{{--                            @method('DELETE')--}}
{{--                            <button type="submit" class="btn btn-danger show_confirm">Delete</button>--}}
{{--                        </form>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--                @endforeach--}}
{{--                </tbody>--}}
{{--            </table>--}}
{{--        </div>--}}
{{--    </div>--}}

<br><br>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5>All categories</h5>
            <a href="{{route('petition.create')}}" class="btn btn-primary">create petition</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="display" id="basic-1">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Status</th>
                        <th>Petition text</th>
                        <th>Portfolio link</th>
                        <th>Result</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($petitions as $key=>$petition)
                        <tr>
                            <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                <strong> {{$key+1}} </strong>
                            </td>
                            <td>
                                @if($petition->status == 0)
                                    <span class="badge badge-warning">Checking</span>
                                @elseif($petition->status == 1)
                                    <span class="badge badge-success">Accept</span>
                                @elseif($petition->status == 2)
                                    <span class="badge badge-danger">Cancel</span>
                                @endif
                            </td>
                            <td>
                                {{substr($petition->petition_text, 0,30)}}..
                            </td>
                            <td>
                                {{substr($petition->portfolio_link, 0, 30)}}..
                            </td>
                            <td>
                                    <div class="">
                                        <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#basicModal">
                                            result
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel1">Message from Administrator</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col mb-3">
                                                                <p class="">{{$petition->result_description}}</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                                            Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </td>
                            <td class="d-flex">
                                <a href="{{route('petition.edit', $petition)}}" class="btn btn-primary">Edit</a>
                                &nbsp;<form action="{{route('petition.destroy', $petition)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger show_confirm">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete petition?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>

@endpush
