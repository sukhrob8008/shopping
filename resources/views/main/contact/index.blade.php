<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bog'lanish</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
          rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('main/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/style.css')}}" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body>


<!-- Header Section Begin -->
<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7">
                    <div class="header__top__left">
                        <p>3D modellar online do'koniga xush kelibsiz!</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="header__top__right">
                        <div class="header__top__hover">
                            <span>Tilni o'zgartirish <i class="arrow_carrot-down"></i></span>
                            <ul style="width: 100px; text-align: center">
                                <li>UZ</li>
                                <li>EN</li>
                                <li>RU</li>
                            </ul>
                        </div>
                        <div class="header__top__links">
                            <a style="text-transform: lowercase" class="btn btn-success" href="{{route('login')}}">login</a>
                        </div>
                        <div class="header__top__links">
                            <a style="text-transform: lowercase" class="btn btn-primary" href="{{route('register')}}">register</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="{{route('main.page')}}"><img src="{{asset('main/img/logo.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <nav class="header__menu mobile-menu">
                    <ul>
                        <li><a href="">Blog</a></li>
                        <li class="active"><a href="">Bog'lanish</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="header__nav__option">
                    <a href="#" class="search-switch"><img src="{{asset('main/img/icon/search.png')}}" alt=""></a>
                    <a href="#"><img src="{{asset('main/img/icon/heart.png')}}" alt=""></a>
                    <a href="#"><img src="{{asset('main/img/icon/cart.png')}}" alt=""> <span>0</span></a>
                    <div class="price">$0.00</div>
                </div>
            </div>
        </div>
        <div class="canvas__open"><i class="fa fa-bars"></i></div>
    </div>
</header>

<!-- Header Section End -->

<!-- Map Begin -->
<div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d111551.9926412813!2d-90.27317134641879!3d38.606612219170856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2sbd!4v1597926938024!5m2!1sen!2sbd" height="500" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
<!-- Map End -->

<!-- Contact Section Begin -->
<section class="contact spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="contact__text">
                    <div class="section-title">
                        <h2>Biz bilan bog'laning</h2>
                        <p>As you might expect of a company that began as a high-end interiors contractor, we pay
                            strict attention.</p>
                    </div>
                    <ul>
                        <li>
                            <h4>O'zbekiston</h4>
                            <p>19 uy, Toshkent shahri, Yunusobod t. <br/>+43 982-314-0958</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="contact__form">
                    <form action="{{route('contact.store')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <input class="@error('name') is-invalid @enderror" name="name" type="text" placeholder="Ismingiz">
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-6">
                                <input class="@error('email') is-invalid @enderror" name="email" type="email" placeholder="Pochta manzilingiz">
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-12">
                                <textarea class="@error('text') is-invalid @enderror" name="text" placeholder="Xabar matni"></textarea>
                                @error('text')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <button type="submit" class="site-btn">Jo'natish</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Section End -->

<!-- Footer Section Begin -->
<div class="row">
    <div class="col-lg-12 text-center">
        <div class="footer__copyright__text">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <p>Soft Go ©
                <script>
                    document.write(new Date().getFullYear());
                </script>
                All rights reserved <i aria-hidden="true"></i> by <a href="https://softgo.uz" target="_blank">soft go</a>
            </p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>
    </div>
</div>
<!-- Footer Section End -->


<!-- Js Plugins -->
<script src="{{asset('main/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('main/js/bootstrap.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('main/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('main/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('main/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('main/js/mixitup.min.js')}}"></script>
<script src="{{asset('main/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('main/js/main.js')}}"></script>
@include('sweetalert::alert')
</body>

</html>
