<div class="col-lg-3">
    <div class="shop__sidebar">
        <div class="shop__sidebar__search">
            <form action="#">
                <input type="text" placeholder="Search...">
                <button type="submit"><span class="icon_search"></span></button>
            </form>
        </div>
        <div class="shop__sidebar__accordion">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseOne">Categories</a>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="shop__sidebar__categories">
                                <ul class="nice-scroll">
                                    @foreach($categories as $key => $category)
{{--                                        @if($category->id == $key)--}}
{{--                                            <li><a style="color: {{(request()->routeIs('category.filter')) ? '#6600FF' : ''}}" href="{{route('category.filter', $category->id)}}">{{$category->category_name}} ({{$category_count[$key]}})</a></li>--}}
{{--                                        @else--}}
                                            <li><a href="{{route('category.filter', $category->id)}}">{{$category->category_name}} ({{$category_count[$key]}})</a></li>
{{--                                        @endif--}}
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseTwo">Subcategories</a>
                    </div>
                    <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="shop__sidebar__brand">
                                <ul>
                                    @foreach($subcategories as $key => $subcategory)
                                        <li><a href="{{route('subcategory.filter', $subcategory->id)}}">{{$subcategory->subcategory_name}}&nbsp;({{$subcategory_count[$key]}})</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseThreePrice">Narxlar</a>
                    </div>
                    <div id="collapseThreePrice" class="collapse" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="shop__sidebar__price">
                                <ul>
                                    <li><a href="{{route('price.filter', 5000)}}">0 - 5000 so'm</a></li>
                                    <li><a href="{{route('price.filter', 10000)}}">5000 - 10000 so'm</a></li>
                                    <li><a href="{{route('price.filter', 20000)}}">10000 - 20000 so'm</a></li>
                                    <li><a href="{{route('price.filter', 50000)}}">20000 - 50000 so'm</a></li>
                                    <li><a href="{{route('price.filter', 60000)}}">+50000 so'm</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseThreeSize">Hajmi</a>
                    </div>
                    <div id="collapseThreeSize" class="collapse" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="shop__sidebar__price">
                                <ul>
                                    <li><a href="{{route('size.filter', 10)}}">0 - 10 mb</a></li>
                                    <li><a href="{{route('size.filter', 30)}}">10 - 30 mb</a></li>
                                    <li><a href="{{route('size.filter', 100)}}">30 - 100 mb</a></li>
                                    <li><a href="{{route('size.filter', 110)}}">+100 mb</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="card">--}}
{{--                    <div class="card-heading">--}}
{{--                        <a data-toggle="collapse" data-target="#collapseFour">Size</a>--}}
{{--                    </div>--}}
{{--                    <div id="collapseFour" class="collapse show" data-parent="#accordionExample">--}}
{{--                        <div class="card-body">--}}
{{--                            <div class="shop__sidebar__size">--}}
{{--                                <label for="xs">xs--}}
{{--                                    <input type="radio" id="xs">--}}
{{--                                </label>--}}
{{--                                <label for="sm">s--}}
{{--                                    <input type="radio" id="sm">--}}
{{--                                </label>--}}
{{--                                <label for="md">m--}}
{{--                                    <input type="radio" id="md">--}}
{{--                                </label>--}}
{{--                                <label for="xl">xl--}}
{{--                                    <input type="radio" id="xl">--}}
{{--                                </label>--}}
{{--                                <label for="2xl">2xl--}}
{{--                                    <input type="radio" id="2xl">--}}
{{--                                </label>--}}
{{--                                <label for="xxl">xxl--}}
{{--                                    <input type="radio" id="xxl">--}}
{{--                                </label>--}}
{{--                                <label for="3xl">3xl--}}
{{--                                    <input type="radio" id="3xl">--}}
{{--                                </label>--}}
{{--                                <label for="4xl">4xl--}}
{{--                                    <input type="radio" id="4xl">--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseFive">Colors</a>
                    </div>
                    <div id="collapseFive" class="collapse show" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="shop__sidebar__color">
                                <label class="c-1" for="sp-1">
                                    <input type="radio" id="sp-1">
                                </label>
                                <label class="c-2" for="sp-2">
                                    <input type="radio" id="sp-2">
                                </label>
                                <label class="c-3" for="sp-3">
                                    <input type="radio" id="sp-3">
                                </label>
                                <label class="c-4" for="sp-4">
                                    <input type="radio" id="sp-4">
                                </label>
                                <label class="c-5" for="sp-5">
                                    <input type="radio" id="sp-5">
                                </label>
                                <label class="c-6" for="sp-6">
                                    <input type="radio" id="sp-6">
                                </label>
                                <label class="c-7" for="sp-7">
                                    <input type="radio" id="sp-7">
                                </label>
                                <label class="c-8" for="sp-8">
                                    <input type="radio" id="sp-8">
                                </label>
                                <label class="c-9" for="sp-9">
                                    <input type="radio" id="sp-9">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseSix">Tags</a>
                    </div>
                    <div id="collapseSix" class="collapse" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="shop__sidebar__tags">
                                @foreach($product_types as $product_type)
                                    <a href="{{route('type.filter', $product_type->model_type)}}">{{$product_type->model_type}}&nbsp;({{$product_type->count}})</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
