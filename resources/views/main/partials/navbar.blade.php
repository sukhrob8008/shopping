<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7">
                    <div class="header__top__left">
                        <p>3D modellar online do'koniga xush kelibsiz!</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="header__top__right">
                        <div class="header__top__hover">
                            <span>Tilni o'zgartirish <i class="arrow_carrot-down"></i></span>
                            <ul style="width: 100px; text-align: center">
                                <li>UZ</li>
                                <li>EN</li>
                                <li>RU</li>
                            </ul>
                        </div>
                        <div class="header__top__links">
                            <a style="text-transform: lowercase" class="btn btn-success" href="{{route('login')}}">login</a>
                        </div>
                        <div class="header__top__links">
                            <a style="text-transform: lowercase" class="btn btn-primary" href="{{route('register')}}">register</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="{{route('main.page')}}"><img src="{{asset('main/img/logo.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <nav class="header__menu mobile-menu">
                    <ul>
{{--                        <li><a href="">Home</a></li>--}}
{{--                        <li class="active"><a href="">Shop</a></li>--}}
{{--                        <li><a href="#">Pages</a>--}}
{{--                            <ul class="dropdown">--}}
{{--                                <li><a href="">About Us</a></li>--}}
{{--                                <li><a href="">Shop Details</a></li>--}}
{{--                                <li><a href="">Shopping Cart</a></li>--}}
{{--                                <li><a href="">Check Out</a></li>--}}
{{--                                <li><a href="">Blog Details</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
                        <li><a href="">Blog</a></li>
                        <li><a href="{{route('main.contact')}}">Bog'lanish</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="header__nav__option">
                    <a href="#" class="search-switch"><img src="{{asset('main/img/icon/search.png')}}" alt=""></a>
                    <a href="#"><img src="{{asset('main/img/icon/heart.png')}}" alt=""></a>
                    <a href="#"><img src="{{asset('main/img/icon/cart.png')}}" alt=""> <span>0</span></a>
                    <div class="price">$0.00</div>
                </div>
            </div>
        </div>
        <div class="canvas__open"><i class="fa fa-bars"></i></div>
    </div>
</header>
