<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bog'lanish</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
          rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('main/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/style.css')}}" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body>


<!-- Header Section Begin -->
<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7">
                    <div class="header__top__left">
                        <p>3D modellar online do'koniga xush kelibsiz!</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="header__top__right">
                        <div class="header__top__hover">
                            <span>Tilni o'zgartirish <i class="arrow_carrot-down"></i></span>
                            <ul style="width: 100px; text-align: center">
                                <li>UZ</li>
                                <li>EN</li>
                                <li>RU</li>
                            </ul>
                        </div>
                        <div class="header__top__links">
                            <a style="text-transform: lowercase" class="btn btn-success" href="{{route('login')}}">login</a>
                        </div>
                        <div class="header__top__links">
                            <a style="text-transform: lowercase" class="btn btn-primary" href="{{route('register')}}">register</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="{{route('main.page')}}"><img src="{{asset('main/img/logo.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <nav class="header__menu mobile-menu">
                    <ul>
                        <li><a href="">Blog</a></li>
                        <li><a href="">Bog'lanish</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="header__nav__option">
                    <a href="#" class="search-switch"><img src="{{asset('main/img/icon/search.png')}}" alt=""></a>
                    <a href="#"><img src="{{asset('main/img/icon/heart.png')}}" alt=""></a>
                    <a href="#"><img src="{{asset('main/img/icon/cart.png')}}" alt=""> <span>0</span></a>
                    <div class="price">$0.00</div>
                </div>
            </div>
        </div>
        <div class="canvas__open"><i class="fa fa-bars"></i></div>
    </div>
</header>

<section class="shop-details">
    <div class="product__details__pic">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product__details__breadcrumb">
                        <a href="">Home</a>
                        <a href="">Shop</a>
                        <span>Product Details</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <ul class="nav nav-tabs" role="tablist">
                        @for($i=0; $i<count($medium_images_array)-1; $i++)
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-{{$i+1}}" role="tab">
                                    <div class="product__thumb__pic set-bg" data-setbg="{{asset('architec/models/images/medium/'.$medium_images_array[$i])}}" style="background-image: url(&quot;img/shop-details/thumb-1.png&quot;);">
                                    </div>
                                </a>
                            </li>
                        @endfor
                    </ul>
                </div>
                <div class="col-lg-6 col-md-9">
                    <div class="tab-content">
                            @for($i=0; $i<count($big_images_array)-1; $i++)
                                @if($i == 0)
                                    <div class="tab-pane active" id="tabs-{{$i+1}}" role="tabpanel">
                                        <div class="product__details__pic__item">
                                            <img src="{{asset('architec/models/images/big/'.$big_images_array[$i])}}" alt="">
                                        </div>
                                    </div>
                                @else
                                    <div class="tab-pane" id="tabs-{{$i+1}}" role="tabpanel">
                                        <div class="product__details__pic__item">
                                            <img src="{{asset('architec/models/images/big/'.$big_images_array[$i])}}" alt="">
                                        </div>
                                    </div>
                                @endif
                            @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product__details__content">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="product__details__text">
                        <h4>{{$product_accept->model_name}}</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <span> - 5 Reviews</span>
                        </div>
                        <h3>{{$product_accept->model_price}} so'm<span>{{$product_accept->model_price*1.2}} so'm</span></h3>
                        <p>{{$product_accept->model_description}}</p>
                        <div class="product__details__option d-flex">
                            <div class="product__details__option__size">
                                <b><span>Category:</span></b>
                                {{$product_accept->category->category_name}}
                            </div>
                            <div class="product__details__option__size">
                                <b><span>Subcategory:</span></b>
                                {{$product_accept->subcategory->subcategory_name}}
                            </div>
                            <div class="product__details__option__size">
                                <b><span>Size:</span></b>
                                {{round($product_accept->model_size/1048576, 2)}} mb
                            </div>
                            <div class="product__details__option__size">
                                <b><span>Type:</span></b>
                                .{{$product_accept->model_type}}
                            </div>
                        </div>
{{--                        <div class="product__details__cart__option">--}}
{{--                            <a href="#" class="primary-btn">sotib olish</a>--}}
{{--                        </div>--}}
{{--                        <div class="product__details__btns__option">--}}
{{--                            <a href="#"><i class="fa fa-heart"></i> add to wishlist</a>--}}
{{--                            <a href="#"><i class="fa fa-exchange"></i> Add To Compare</a>--}}
{{--                        </div>--}}
                        <div class="product__details__last__option">
                            <h5><span>Guaranteed Safe Checkout</span></h5>
                            <img width="100" height="100" src="{{asset('main/img/payment/Click-01.png')}}" alt="">
                            <img width="100" height="100" src="{{asset('main/img/payment/payme_01.svg')}}" alt="">
                            <ul>
                                <li><span>SKU:</span> 3812912</li>
                                <li><span>Categories:</span> Clothes</li>
                                <li><span>Tag:</span> Clothes, Skin, Body</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-5" role="tab" aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-6" role="tab" aria-selected="false">Customer
                                    Previews(5)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-7" role="tab">Additional
                                    information</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-5" role="tabpanel">
                                <div class="product__details__tab__content">
                                    <p class="note">Nam tempus turpis at metus scelerisque placerat nulla deumantos
                                        solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis
                                        ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo
                                        pharetras loremos.</p>
                                    <div class="product__details__tab__content__item">
                                        <h5>Products Infomation</h5>
                                        <p>A Pocket PC is a handheld computer, which features many of the same
                                            capabilities as a modern PC. These handy little devices allow
                                            individuals to retrieve and store e-mail messages, create a contact
                                            file, coordinate appointments, surf the internet, exchange text messages
                                            and more. Every product that is labeled as a Pocket PC must be
                                            accompanied with specific software to operate the unit and must feature
                                            a touchscreen and touchpad.</p>
                                        <p>As is the case with any new technology product, the cost of a Pocket PC
                                            was substantial during it’s early release. For approximately $700.00,
                                            consumers could purchase one of top-of-the-line Pocket PCs in 2003.
                                            These days, customers are finding that prices have become much more
                                            reasonable now that the newness is wearing off. For approximately
                                            $350.00, a new Pocket PC can now be purchased.</p>
                                    </div>
                                    <div class="product__details__tab__content__item">
                                        <h5>Material used</h5>
                                        <p>Polyester is deemed lower quality due to its none natural quality’s. Made
                                            from synthetic materials, not natural like wool. Polyester suits become
                                            creased easily and are known for not being breathable. Polyester suits
                                            tend to have a shine to them compared to wool and cotton suits, this can
                                            make the suit look cheap. The texture of velvet is luxurious and
                                            breathable. Velvet is a great choice for dinner party jacket and can be
                                            worn all year round.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-6" role="tabpanel">
                                <div class="product__details__tab__content">
                                    <div class="product__details__tab__content__item">
                                        <h5>Products Infomation</h5>
                                        <p>A Pocket PC is a handheld computer, which features many of the same
                                            capabilities as a modern PC. These handy little devices allow
                                            individuals to retrieve and store e-mail messages, create a contact
                                            file, coordinate appointments, surf the internet, exchange text messages
                                            and more. Every product that is labeled as a Pocket PC must be
                                            accompanied with specific software to operate the unit and must feature
                                            a touchscreen and touchpad.</p>
                                        <p>As is the case with any new technology product, the cost of a Pocket PC
                                            was substantial during it’s early release. For approximately $700.00,
                                            consumers could purchase one of top-of-the-line Pocket PCs in 2003.
                                            These days, customers are finding that prices have become much more
                                            reasonable now that the newness is wearing off. For approximately
                                            $350.00, a new Pocket PC can now be purchased.</p>
                                    </div>
                                    <div class="product__details__tab__content__item">
                                        <h5>Material used</h5>
                                        <p>Polyester is deemed lower quality due to its none natural quality’s. Made
                                            from synthetic materials, not natural like wool. Polyester suits become
                                            creased easily and are known for not being breathable. Polyester suits
                                            tend to have a shine to them compared to wool and cotton suits, this can
                                            make the suit look cheap. The texture of velvet is luxurious and
                                            breathable. Velvet is a great choice for dinner party jacket and can be
                                            worn all year round.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-7" role="tabpanel">
                                <div class="product__details__tab__content">
                                    <p class="note">Nam tempus turpis at metus scelerisque placerat nulla deumantos
                                        solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis
                                        ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo
                                        pharetras loremos.</p>
                                    <div class="product__details__tab__content__item">
                                        <h5>Products Infomation</h5>
                                        <p>A Pocket PC is a handheld computer, which features many of the same
                                            capabilities as a modern PC. These handy little devices allow
                                            individuals to retrieve and store e-mail messages, create a contact
                                            file, coordinate appointments, surf the internet, exchange text messages
                                            and more. Every product that is labeled as a Pocket PC must be
                                            accompanied with specific software to operate the unit and must feature
                                            a touchscreen and touchpad.</p>
                                        <p>As is the case with any new technology product, the cost of a Pocket PC
                                            was substantial during it’s early release. For approximately $700.00,
                                            consumers could purchase one of top-of-the-line Pocket PCs in 2003.
                                            These days, customers are finding that prices have become much more
                                            reasonable now that the newness is wearing off. For approximately
                                            $350.00, a new Pocket PC can now be purchased.</p>
                                    </div>
                                    <div class="product__details__tab__content__item">
                                        <h5>Material used</h5>
                                        <p>Polyester is deemed lower quality due to its none natural quality’s. Made
                                            from synthetic materials, not natural like wool. Polyester suits become
                                            creased easily and are known for not being breathable. Polyester suits
                                            tend to have a shine to them compared to wool and cotton suits, this can
                                            make the suit look cheap. The texture of velvet is luxurious and
                                            breathable. Velvet is a great choice for dinner party jacket and can be
                                            worn all year round.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="row">
    <div class="col-lg-12 text-center">
        <div class="footer__copyright__text">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <p>Soft Go ©
                <script>
                    document.write(new Date().getFullYear());
                </script>
                All rights reserved <i aria-hidden="true"></i> by <a href="https://softgo.uz" target="_blank">soft go</a>
            </p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>
    </div>
</div>
<!-- Footer Section End -->


<!-- Js Plugins -->
<script src="{{asset('main/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('main/js/bootstrap.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('main/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('main/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('main/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('main/js/mixitup.min.js')}}"></script>
<script src="{{asset('main/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('main/js/main.js')}}"></script>
@include('sweetalert::alert')
</body>

</html>
