@extends('admin.layouts.master')
@section('title', 'Update subcategory')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('subcategory.update',$subcategory)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <h5 class="card-title">Update subcategory</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Select category</div>
                                <select name="category_id" class="js-example-basic-single col-sm-12 select2-hidden-accessible @error('category_id') is-invalid @enderror" tabindex="-1" aria-hidden="true">
                                    <option selected disabled>select category</option>
                                    @foreach($categories as $category)
                                        @if($category->id == $subcategory->category_id)
                                        {
                                            <option selected value="{{$category->id}}">{{$category->category_name}}</option>
                                        }@else
                                        {
                                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        }
                                        @endif
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br>
                            <div class="col-form-label">subcategory name</div>
                            <div class="mb-3">
                                <input value="{{$subcategory->subcategory_name}}" type="text" name="subcategory_name" class="form-control @error('subcategory_name') is-invalid @enderror" placeholder="subcategory name">
                                @error('subcategory_name')
                                <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="update subcategory">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
