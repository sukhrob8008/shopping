@extends('admin.layouts.master')
@section('title', 'Add subcategory')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('subcategory.store')}}" method="post">
                        @csrf
                        <div class="card-header">
                            <h5 class="card-title">Add subcategory</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Select category</div>
                                <select name="category_id" class="js-example-basic-single col-sm-12 select2-hidden-accessible @error('category_id') is-invalid @enderror" tabindex="-1" aria-hidden="true">
                                    <option selected disabled>select category</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br>
                            <div class="col-form-label">subcategory name</div>
                            <div class="mb-3 d-flex">
                                <input style="margin-right: 20px" type="text" name="subcategory_name[]" class="form-control w-85 @error('subcategory_name') is-invalid @enderror" placeholder="subcategory name">
                                    @error('subcategory_name')
                                        <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                <button style="background:#0f6e3a; color: white; font-weight: bold; float: right; margin-left: 20px" id="add_btn" class="btn">+</button>
                            </div>
                            <div id="child">

                            </div>

                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="add subcategory">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function (){
            $('#add_btn').click(function (e){
                e.preventDefault();
                $('#child').append(`

                        <div class="mb-3 d-flex">
                            <input style="margin-right: 20px" type="text" name="subcategory_name[]" class="form-control w-85" placeholder="subcategory name">
                            <button style="background:#871209; color: white; font-weight: bold; float: right; margin-left: 20px" id="remove_btn" class="btn">x</button>
                        </div>

                    `);
            });

            $(document).on('click', '#remove_btn', function (e) {
                e.preventDefault();
                let row_item = $(this).parent();
                $(row_item).remove();
            });

        });
    </script>

@endpush
