@extends('admin.layouts.master')
@section('title','View categories')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>All categories</h5>
                <a href="{{route('subcategory.create')}}" class="btn btn-primary">create subcategory</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Subcategory name</th>
                            <th>Created date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subcategories as $key=>$subcategory)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$subcategory->category_name->category_name}}</td>
                                <td>{{$subcategory->subcategory_name}}</td>
                                <td>{{$subcategory->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('subcategory.edit',$subcategory)}}" class="btn btn-success">Edit</a>
                                    &nbsp;<form action="{{route('subcategory.destroy', $subcategory)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger show_confirm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Do you want delete subcategory?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>

@endpush
