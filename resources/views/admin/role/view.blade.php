@extends('admin.layouts.master')
@section('title','View roles')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>All users</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $key=>$role)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$role->user_info->name}}</td>
                                    <td>{{$role->user_info->lname}}</td>
                                    <td>{{$role->user_info->email}}</td>
                                    <td>
                                        @if($role->role_id == 1)
                                            Admin
                                        @elseif($role->role_id == 2)
                                            Architector
                                        @elseif($role->role_id == 3)
                                            User
                                        @endif
                                    </td>
                                    <td class="d-flex">
                                        <a href="{{route('role.edit',$role)}}" class="btn btn-success">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
