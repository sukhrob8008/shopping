@extends('admin.layouts.master')
@section('title', 'Update role')
@section('content')
    <div class="select2-drpdwn">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-3">
                    <form action="{{route('role.update',$role)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <h5 class="card-title">Update user role</h5>
                        </div>
                        <div class="card-body o-hidden">
                            <div class="mb-2">
                                <div class="col-form-label">Select user</div>
                                <select name="user_id" class="js-example-basic-single col-sm-12 select2-hidden-accessible @error('user_id') is-invalid @enderror" tabindex="-1" aria-hidden="true">
                                    <option selected value="{{$user_id}}">{{$user_name.' '.$user_lname}}</option>
                                </select>
                                @error('user_id')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-2">
                                <div class="col-form-label">Select role</div>
                                <select name="role_id" class="js-example-basic-single col-sm-12 select2-hidden-accessible @error('role_id') is-invalid @enderror" tabindex="-1" aria-hidden="true">
                                    <option selected disabled>select role</option>
                                    @if($user_role==1)
                                        <option selected value="1">Administrator</option>
                                        <option value="2">Architector</option>
                                        <option value="3">User</option>
                                    @elseif($user_role==2)
                                        <option  value="1">Administrator</option>
                                        <option selected value="2">Architector</option>
                                        <option value="3">User</option>
                                    @elseif($user_role==3)
                                        <option  value="1">Administrator</option>
                                        <option  value="2">Architector</option>
                                        <option selected value="3">User</option>
                                    @else
                                        <option value="1">Administrator</option>
                                        <option value="2">Architector</option>
                                        <option value="3">User</option>
                                    @endif
                                </select>
                                @error('role_id')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br>
                            <div class="mb-2">
                                <input type="submit" class="btn btn-primary" value="add role">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
