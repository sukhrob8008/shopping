@extends('admin.layouts.master')
@section('title','Messanger')
@section('content')
    <br>
    <div class="row">
        <div class="col call-chat-sidebar col-sm-12">
            <div class="card">
                <div class="card-body chat-body">
                    <div class="chat-box">
                        <!-- Chat left side Start-->
                        <div class="chat-left-aside">
                            <div class="media"><img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/12.png')}}" alt="">
                                <div class="about">
                                    <div class="name f-w-600">{{\Illuminate\Support\Facades\Auth::user()->name.' '.\Illuminate\Support\Facades\Auth::user()->lname}}</div>
                                    <div class="status">Status...</div>
                                </div>
                            </div>
                            <div class="people-list" id="people-list">
                                <div class="search">
                                        <div class="mb-3">
                                            <input id="search" class="form-control" type="text" placeholder="Search" data-bs-original-title="" title=""><i class="fa fa-search"></i>
                                        </div>
                                </div>
                                <ul class="list" id="members_list">
                                    @foreach($members as $member)
                                    <li class="clearfix"><img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/5.jpg')}}" alt="">
                                        <a href="{{route('members.chat',$member)}}">
                                            <div class="status-circle away"></div>
                                            <div class="about">
                                                <div class="name">{{$member->name.' '.$member->lname}}</div>
                                                <div class="status">
                                                    @if($member->role==2)
                                                        Architector
                                                    @elseif($member->role==3)
                                                        User
                                                    @endif
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- Chat left side Ends-->
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('script')

    <script>
        $(document).ready(function (){


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var member_chat =  "{{route('members.chat', ':member')}}";

            $('#search').keyup(function (){
                var search = $('#search').val();
                var members_list = $('#members_list');
                $.ajax({
                    url: '{{route('members.search')}}',
                    method: 'POST',
                    data: {
                        search: search,
                    },
                    success: function (response)
                    {
                        var role = '';
                        $('#members_list').html("");
                        for(i=0; i<response.result.length; i++)
                        {
                            if (response.result[i].role == 2)
                            {
                                role = 'Architector'
                            }else {
                                role = 'User'
                            }
                            $('#members_list').append(`

                               <li class="clearfix"><img class="rounded-circle user-image" src="{{asset('admin/assets/images/user/5.jpg')}}" alt="">
                                      <a href="${member_chat.replace(':member', response.result[i].id)}">
                                            <div class="status-circle away"></div>
                                            <div class="about">
                                                <div class="name">${response.result[i].name+' '+response.result[i].lname}</div>
                                                <div class="status">
                                                ${role}
                                            </div>
                                        </div>
                                    </a>
                                </li>
`);
                        }
                    }
                })
            });
        });
    </script>

@endpush
