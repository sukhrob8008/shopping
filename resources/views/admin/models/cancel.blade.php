@extends('admin.layouts.master')
@section('title','Accepting models')
@section('content')

    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Accepting 3D models table</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Architector</th>
                            <th>Model name</th>
                            <th>Model price</th>
                            <th>View more</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $key => $product)
                            <tr>
                                <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                    <strong> {{$key+1}} </strong>
                                </td>
                                <td>
                                    {{$product->members->name.' '.$product->members->lname}}
                                </td>
                                <td>
                                    {{$product->model_name}}
                                </td>
                                <td>
                                    {{$product->model_price}}&nbsp;sum
                                </td>
                                <td>
                                    <a href="{{route('product.show_receive', $product)}}" class="btn btn-dark">details</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')

    <script>

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            });

            {{--$('.check_status').change(function () {--}}
            {{--    var status = 0;--}}
            {{--    if(this.checked) {--}}
            {{--        status = 1;--}}
            {{--    }else {--}}
            {{--        status = 0;--}}
            {{--    }--}}
            {{--    let product_id = $(this).attr('data-product-id');--}}
            {{--    $.ajax({--}}
            {{--       url: '{{route('product.status')}}',--}}
            {{--       method: 'POST',--}}
            {{--       data:{--}}
            {{--           'status':status,--}}
            {{--           'product_id':product_id--}}
            {{--       },--}}
            {{--       success:function (response){--}}

            {{--       }--}}
            {{--    });--}}

            {{--});--}}




            {{--$.ajax({--}}
            {{--    url:'{{route('architec.category')}}',--}}
            {{--    method: 'POST',--}}
            {{--    data:{--}}
            {{--        'category_id' : category_id--}}
            {{--    },--}}
            {{--    success:function (response) {--}}

            {{--        for (i=0; i<response.subcategories.length; i++)--}}
            {{--        {--}}
            {{--            $("#subcategory_id").append('<option value='+response.subcategories[i].id+'>'+response.subcategories[i].subcategory_name+'</option>');--}}
            {{--        }--}}
            {{--    },--}}
            {{--    error:function (response) {--}}
            {{--        console.log(response)--}}
            {{--    }--}}
            {{--});--}}
            // });


        });

    </script>
@endpush

