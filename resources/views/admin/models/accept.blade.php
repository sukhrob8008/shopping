@extends('admin.layouts.master')
@section('title','Accepting models')
@section('content')

    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>Accepting 3D models table</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Architector</th>
                            <th>Model name</th>
                            <th>Model price</th>
                            <th>View more</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $key => $product)
                            <tr>
                                <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                    <strong> {{$key+1}} </strong>
                                </td>
                                <td>
                                    {{$product->members->name.' '.$product->members->lname}}
                                </td>
                                <td>
                                    {{$product->model_name}}
                                </td>
                                <td>
                                    {{$product->model_price}}&nbsp;sum
                                </td>
                                <td>
                                    <a href="{{route('product.show_receive', $product)}}" class="btn btn-dark">details</a>
                                </td>
                                <td>
                                    <div class="col-sm-8 col-6">
                                        <div class="media-body text-start icon-state">
                                            <label class="switch">
                                                @if($product->block == 1 && $product->status == 1)
                                                <input checked class="check_status" name="check_status" id="check_status" data-product-id="{{$product->id}}" value="1" type="checkbox"><span class="switch-state"></span>
                                                @elseif($product->block == 0 && $product->status == 1)
                                                    <input class="check_status" name="check_status" id="check_status" data-product-id="{{$product->id}}" value="0" type="checkbox"><span class="switch-state"></span>
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')

    <script>

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.check_status').change(function () {
                var status = 0;
                if(this.checked) {
                    status = 1;
                }else {
                    status = 0;
                }
                let product_id = $(this).attr('data-product-id');
                $.ajax({
                   url: '{{route('product.block')}}',
                   method: 'POST',
                   data:{
                       'status':status,
                       'product_id':product_id
                   },
                   success:function (response){

                   }
                });

            });
        });

    </script>
@endpush

