@extends('admin.layouts.master')
@section('title','Show notification')
@section('content')
    <br><br>
    <div class="col-xl-12 xl-100 box-col-6">
        <div class="card">
            <div class="card-body">
                <div class="product-page-details">
                    <h3>{{$petition->user_name->name.' '.$petition->user_name->lname}}</h3>
                </div>
                <ul class="product-color">
                    <li class="bg-primary"></li>
                    <li class="bg-secondary"></li>
                    <li class="bg-success"></li>
                    <li class="bg-info"></li>
                    <li class="bg-warning"></li>
                </ul>
                <hr>
                <p>{{$petition->petition_text}}</p>
                <hr>
                <div>
                    <table class="product-page-width">
                        <tbody>
                        <tr>
                            <td><b>Arrive date &nbsp;&nbsp;&nbsp;:</b></td>
                            <td>{{$petition->created_at}}</td>
                        </tr>
                        <tr>
                            <td> <b>Email &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b></td>
                            <td class="txt-success">{{$petition->user_name->email}}</td>
                        </tr>
                        <tr>
                            <td> <b>Portfolio &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b></td>
                            <td>{{$petition->portfolio_link}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="m-t-15">
{{--                    <button class="btn btn-success m-r-10" type="button" title="" data-bs-original-title=""> <i class="fa fa-shopping-cart me-1"></i>Accept</button>--}}
{{--                    <a class="btn btn-success" href="" data-bs-original-title="" title=""> <i class="fa fa-heart me-1"></i>Cancel</a>--}}
                    <!-- accept-->
                    <button class="btn btn-success" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalfat" data-whatever="@mdo" data-bs-original-title="" title="">Accept</button>
                    <div class="modal fade" id="exampleModalfat" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel2">Do you accept for this user?</h5>
                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                    <a class="btn btn-primary" href="{{route('dashboard.notification_accept', $petition)}}" data-bs-original-title="" title="">Accept</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- cancel-->
                    <button class="btn btn-danger" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalfatCancel" data-whatever="@mdo" data-bs-original-title="" title="">Cancel</button>
                    <div class="modal fade" id="exampleModalfatCancel" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel2">Send <span class="badge badge-danger">cancel</span> description to user</h5>
                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
                                </div>
                                <form action="{{route('dashboard.notification_cancel', $petition)}}" method="post">
                                    @csrf

                                    <div class="modal-body">

                                            <div class="mb-3">
                                                <label class="col-form-label" for="message-text">Message:</label>
                                                <textarea name="result_description" class="form-control" id="message-text"></textarea>
                                            </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                        <button class="btn btn-primary" type="submit" data-bs-original-title="" title="">Send message</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
