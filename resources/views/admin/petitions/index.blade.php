@extends('admin.layouts.master')
@section('title', 'View all petitions')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>All petitions</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Last name</th>
                            <th>Petition text</th>
                            <th>Show</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($petitions as $key=>$petition)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$petition->user_name->name}}</td>
                                <td>{{$petition->user_name->lname}}</td>
                                <td>{{$petition->petition_text}}</td>
                                <td class="d-flex">
                                    <a href="{{route('dashboard.notificationshow',$petition)}}" class="btn btn-primary">Show</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
