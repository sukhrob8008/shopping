@extends('admin.layouts.master')
@section('title','Architectors')
@section('content')
    <br><br>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>In your platform registrated <span class="badge badge-success">{{$architectors_count}}</span> architectors</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Architector name</th>
                            <th>Architector lastname</th>
                            <th>Architector email</th>
                            <th>Rating</th>
                            <th>Spam</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($architectors as $key=>$architector)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$architector->name}}</td>
                                <td>{{$architector->lname}}</td>
                                <td>{{$architector->email}}</td>
                                <td><i data-feather="star"></i><i data-feather="star"></i><i data-feather="star"></i><i data-feather="star"></i><i data-feather="star"></i></td>
                                <td>
                                    @if($architector->spam_status == 0)
                                            <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalfatCancel" data-whatever="@mdo" data-bs-original-title="" title="">no spam</button>
                                            <div class="modal fade" id="exampleModalfatCancel" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel2">Send <span class="badge badge-danger">spam</span> description to architector</h5>
                                                            <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
                                                        </div>
                                                        <form action="{{route('architec.spam', $architector)}}" method="post">
                                                            @csrf
                                                            @method('POST')
                                                            <div class="modal-body">
                                                                <div class="mb-3">
                                                                    <label class="col-form-label" for="message-text">Message:</label>
                                                                    <textarea name="spam_reason" class="form-control" id="message-text"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
                                                                <button class="btn btn-primary" type="submit" data-bs-original-title="" title="">Send message</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                    @elseif($architector->spam_status == 1)
                                        <a href="{{route('architec.nospam', $architector)}}" class="btn btn-danger">spam</a>
                                    @endif
                                </td>
                                <td class="d-flex">
                                    <a href="" class="btn btn-success">Edit</a>
                                    &nbsp;<form action="" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger show_confirm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
