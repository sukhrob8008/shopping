<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper"><a href="index.html"><img class="img-fluid for-light" src="{{asset('admin/assets/images/logo/logo.png')}}" alt=""><img class="img-fluid for-dark" src="{{asset('admin/assets/images/logo/logo_dark.png')}}" alt=""></a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="index.html"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6 class="lan-1">Moduls</h6>
                            <p class="lan-2">Dashboards,widgets & layout.</p>
                        </div>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user-check"></i><span class="">Roles</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/role/create') ||  request()->is('dashboard/role') ||  request()->is('dashboard/role/*/edit')) ? 'd-block' : ''}}">
                            <li><a class="" href="{{route('role.create')}}" style="color: {{request()->is('dashboard/role/create') ? '#6600FF' : ''}}">Add role</a></li>
                            <li><a class="" href="{{route('role.index')}}" style="color: {{(request()->is('dashboard/role') ||  request()->is('dashboard/role/*/edit')) ? '#6600FF' : ''}}">View roles</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="layers"></i><span class="">Categories</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/category/create') ||  request()->is('dashboard/category') ||  request()->is('dashboard/category/*/edit')) ? 'd-block' : ''}}">
                            <li><a class="" href="{{route('category.create')}}" style="color: {{request()->is('dashboard/category/create') ? '#6600FF' : ''}}">Add category</a></li>
                            <li><a class="" href="{{route('category.index')}}" style="color: {{(request()->is('dashboard/category') ||  request()->is('dashboard/category/*/edit')) ? '#6600FF' : ''}}">View categories</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="list"></i><span class="">Subcategories</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/subcategory/create') ||  request()->is('dashboard/subcategory') ||  request()->is('dashboard/subcategory/*/edit')) ? 'd-block' : ''}}">
                            <li><a class="" href="{{route('subcategory.create')}}" style="color: {{request()->is('dashboard/subcategory/create') ? '#6600FF' : ''}}">Add subcategory</a></li>
                            <li><a class="" href="{{route('subcategory.index')}}" style="color: {{(request()->is('dashboard/subcategory') ||  request()->is('dashboard/subcategory/*/edit')) ? '#6600FF' : ''}}">View subcategories</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="users"></i><span class="">Members</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/architectors') ||  request()->is('dashboard/users')) ? 'd-block' : ''}}">
                            <li><a class="" href="{{route('dashboard.architectors')}}" style="color: {{request()->is('dashboard/architectors') ? '#6600FF' : ''}}">View architectors</a></li>
                            <li><a class="" href="{{route('dashboard.users')}}" style="color: {{(request()->is('dashboard/users') ||  request()->is('dashboard/users/*')) ? '#6600FF' : ''}}">View users</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list"><label class="badge badge-danger">{{$receive_products_count}}</label>
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="box"></i><span class="">Receivies 3D`</span></a>
                        <ul class="sidebar-submenu {{(request()->routeIs('product.recieve') || request()->routeIs('product.cancel') || request()->routeIs('product.show_receive') || request()->routeIs('product.accept')) ? 'd-block' : ''}}">
                            <li><a class="" href="{{route('product.recieve')}}" style="color: {{(request()->routeIs('product.recieve')) ? '#6600FF' : ''}}">View receiving models</a></li>
                            <li><a class="" href="{{route('product.accept')}}" style="color: {{(request()->routeIs('product.accept')) ? '#6600FF' : ''}}">View accepting model</a></li>
                            <li><a class="" href="{{route('product.cancel')}}" style="color: {{(request()->routeIs('product.cancel')) ? '#6600FF' : ''}}">View canceling model</a></li>
                        </ul>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="users"></i><span class="">Messeges from users
                                </span></a>
                        <ul class="sidebar-submenu {{(request()->routeIs('recieve.contact')) ? 'd-block' : ''}}">
                            <li><a class="" href="{{route('recieve.contact')}}" style="color: {{(request()->routeIs('recieve.contact')) ? '#6600FF' : ''}}">View receiving messages</a></li>

                        </ul>
                    </li>
                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
