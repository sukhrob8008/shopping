<?php
return [


    'sizes' => [
        'extra' => [
            'width' => 1440,
            'height' => 1024
        ],
        'big' => [
            'width' => 1024,
            'height' => 720
        ],
        'medium' => [
            'width' => 720,
            'height' => 360
        ],
        'small' => [
            'width' => 360,
            'height' => 180
        ],
        'thumb' => [
            'width' => 50,
            'height' => 50
        ],
        'd_small' => [
            'width' => 150,
            'height' => 150
        ],
    ]
];

