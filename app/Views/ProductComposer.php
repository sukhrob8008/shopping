<?php

namespace App\Views;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProductComposer
{
    public function compose(View $view)
    {
        $images_for_view = [];
        $product_types = DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type');
        $product_accepts = Product::where('status','=',1)->where('block','=',1)->get();
        for ($i=0; $i<count($product_accepts); $i++)
        {
            $medium_images_array = explode(',',$product_accepts[$i]->model_img_medium);
            $images_for_view[$i] = $medium_images_array[0];
        }

//        dd($images_for_view);
        $view->with([
            'product_types'=>$product_types,
            'product_accepts'=>$product_accepts,
            'images_for_views'=>$images_for_view
        ]);
    }
}
