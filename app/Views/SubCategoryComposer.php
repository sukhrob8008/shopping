<?php

namespace App\Views;

use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\View\View;

class SubCategoryComposer
{
    public function compose(View $view)
    {
        $subcategory_count = [];
        $subcategories = Subcategory::all();
        for ($i=0; $i<count($subcategories); $i++)
        {
            $count_subcategory_from_products = Product::where('subcategory_id','=',$subcategories[$i]->id)->where('status','=',1)->where('block','=',1)->count();
            $subcategory_count[$i]=$count_subcategory_from_products;
        }
        $view->with([
            'subcategories'=>$subcategories,
            'subcategory_count'=>$subcategory_count
        ]);
    }
}
