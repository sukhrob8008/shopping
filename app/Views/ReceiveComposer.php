<?php

namespace App\Views;

use App\Models\Product;
use Illuminate\View\View;

class ReceiveComposer
{
    public function compose(View $view)
    {
        $receive_products_count = Product::where('status','=',0)->count();
        $view->with([
            'receive_products_count'=>$receive_products_count
        ]);
    }

}
