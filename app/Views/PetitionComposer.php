<?php

namespace App\Views;

use App\Models\Petition;
use DateTime;
use Illuminate\View\View;

class PetitionComposer
{
    public function compose(View $view)
    {

        $vaqtlar = [];
        $petitions = Petition::where('status','=',0)->orderBy('id','desc')->limit(3)->get();
        $petitions_count = Petition::where('status','=',0)->count();

        for ($i=0; $i<count($petitions); $i++)
        {
            $current_date = date('Y-m-d h:i:sa');
            $start_datetime = new DateTime($current_date);
            $diff = $start_datetime->diff(new DateTime($petitions[$i]->created_at));

            if ($diff->d >= 1)
            {
                $diff = $diff->d.' day ago';
            }elseif ($diff->d < 1)
            {
                if ($diff->h >= 1)
                {
                    $diff = $diff->h.' hour ago';
                }elseif ($diff->h < 1)
                {
                    $diff = $diff->i.' minut ago';
                }

            }

            $vaqtlar[$i] = $diff;
        }

        $view->with([
            'petitions_count'=>$petitions_count,
            'petitions'=>$petitions,
            'vaqtlar'=>$vaqtlar
        ]);
    }
}
