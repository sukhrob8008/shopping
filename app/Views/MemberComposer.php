<?php

namespace App\Views;

use App\Models\User;
use Illuminate\View\View;

class MemberComposer
{
    public function compose(View $view)
    {
        $members = User::where('role','!=',1)->get();
        $view->with([
            'members'=>$members
        ]);
    }
}
