<?php

namespace App\Views;

use App\Models\Category;
use App\Models\Product;
use Illuminate\View\View;

class CategoryComposer
{
    public function compose(View $view)
    {
        $category_count = [];
        $categories = Category::all();

        for ($i=0; $i<count($categories); $i++)
        {
            $count_category_from_products = Product::where('category_id','=',$categories[$i]->id)->where('status','=',1)->where('block','=',1)->count();
            $category_count[$i]=$count_category_from_products;
        }

//        dd($categories->id);

        $view->with([
            'categories'=>$categories,
            'category_count'=>$category_count
        ]);
    }
}
