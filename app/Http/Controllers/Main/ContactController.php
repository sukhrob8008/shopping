<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ContactController extends Controller
{
    public function contact_page()
    {
        return view('main.contact.index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'text'=>'required',
        ]);
        $name = $request->input('name');
        $email = $request->input('email');
        $text = $request->input('text');

        DB::table('contacts')->insert([
            'name'=>$name,
            'email'=>$email,
            'text'=>$text,
            'created_at'=>date('Y-m-d h:i')
        ]);
        Alert::success('Muvofaqqiyatli yuborildi', 'Sizning xabaringizga javob pochta orqali beriladi.')->autoClose(10000);
        return redirect()->route('main.page');
    }

    public function recieve_contact()
    {
        $contacts = DB::select('select * from contacts');
//        dd($contacts);
        return view('admin.contact_users.index', compact('contacts'));
    }
}
