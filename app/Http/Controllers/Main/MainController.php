<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index()
    {
        $images_for_view = [];
        $product_types = DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type');
        $product_accepts = Product::where('status','=',1)->where('block','=',1)->get();
        for ($i=0; $i<count($product_accepts); $i++)
        {
            $medium_images_array = explode(',',$product_accepts[$i]->model_img_medium);
            $images_for_view[$i] = $medium_images_array[0];
        }
        return view('main.index', compact('product_types','product_accepts','images_for_view'));
    }

    public function category_filter($id)
    {
        $images_for_view = [];
        $product_types = DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type');
        $product_accepts = Product::where('category_id','=',$id)
            ->where('status','=',1)
            ->where('block','=',1)
            ->get();

//        dd($product_filter);
        for ($i=0; $i<count($product_accepts); $i++)
        {
            $medium_images_array = explode(',',$product_accepts[$i]->model_img_medium);
            $images_for_view[$i] = $medium_images_array[0];
        }
        return view('main.index', compact('product_accepts','images_for_view','product_types'));

    }

    public function subcategory_filter($id)
    {
        $images_for_view = [];
        $product_types = DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type');
        $product_accepts = Product::where('subcategory_id','=',$id)
            ->where('status','=',1)
            ->where('block','=',1)
            ->get();

        for ($i=0; $i<count($product_accepts); $i++)
        {
            $medium_images_array = explode(',',$product_accepts[$i]->model_img_medium);
            $images_for_view[$i] = $medium_images_array[0];
        }

        return view('main.index', compact('product_accepts','product_types','images_for_view'));
    }

    public function price_filter($prc)
    {
        if ($prc == 10000)
        {
            $minPrice = 5000;
            $maxPrice = 10000;
        }elseif ($prc == 20000)
        {
            $minPrice = 10000;
            $maxPrice = 20000;
        }elseif ($prc == 50000)
        {
            $minPrice = 20000;
            $maxPrice = 50000;
        }elseif ($prc == 5000)
        {
            $minPrice = 0;
            $maxPrice = 5000;
        }elseif ($prc == 60000)
        {
            $minPrice = 50000;
            $maxPrice = 1000000;
        }
        $images_for_view = [];
        $product_types = DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type');
        $product_accepts = Product::whereBetween('model_price',[$minPrice,$maxPrice])
            ->where('status','=',1)
            ->where('block','=',1)
            ->get();
//        dd($product_accepts);
        for ($i=0; $i<count($product_accepts); $i++)
        {
            $medium_images_array = explode(',',$product_accepts[$i]->model_img_medium);
            $images_for_view[$i] = $medium_images_array[0];
        }

        return view('main.index', compact('product_accepts','images_for_view','product_types'));
    }

    public function size_filter($sz)
    {
        if ($sz == 10)
        {
            $minSize = 0;
            $maxSize = 10485760;
        }elseif ($sz == 30)
        {
            $minSize = 10485760;
            $maxSize = 31457230;
        }elseif ($sz == 100)
        {
            $minSize = 31457230;
            $maxSize = 104857600;
        }elseif ($sz == 110)
        {
            $minSize = 104857600;
            $maxSize = 1048576000;
        }
        $images_for_view = [];
        $product_types = DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type');
        $product_accepts = Product::whereBetween('model_size',[$minSize,$maxSize])
            ->where('status','=',1)
            ->where('block','=',1)
            ->get();
        for ($i=0; $i<count($product_accepts); $i++)
        {
            $medium_images_array = explode(',',$product_accepts[$i]->model_img_medium);
            $images_for_view[$i] = $medium_images_array[0];
        }

        return view('main.index', compact('product_accepts','images_for_view','product_types'));
    }

    public function type_filter($type)
    {
        $images_for_view = [];
        $product_types = DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type');
        $product_accepts = Product::where('model_type','=',$type)
            ->where('status','=',1)
            ->where('block','=',1)
            ->get();
        for ($i=0; $i<count($product_accepts); $i++)
        {
            $medium_images_array = explode(',',$product_accepts[$i]->model_img_medium);
            $images_for_view[$i] = $medium_images_array[0];
        }
        return view('main.index', compact('product_accepts','product_types','images_for_view'));
    }
}
