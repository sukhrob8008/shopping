<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    public function architectors()
    {
        $architectors = User::where('role','=',2)->get();
        $architectors_count = User::where('role','=',2)->count();

        return view('admin.members.architectors', compact('architectors','architectors_count'));
    }

    public function users()
    {
        $users = User::where('role','=',3)->get();
        $users_count = User::where('role','=',3)->count();

        return view('admin.members.users', compact('users', 'users_count'));
    }
}
