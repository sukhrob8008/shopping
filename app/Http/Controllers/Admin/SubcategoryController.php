<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::all();
        return view('admin.subcategories.view', compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.subcategories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
//        dd(count($request->subcategory_name));
        $request->validate([
            'category_id'=>'required',
            'subcategory_name'=>'required'
        ]);
        $subcategories = new Subcategory();
        $category_id = $request->input('category_id');
        for ($i=0; $i<count($request->subcategory_name); $i++)
        {
            $subcategories = new Subcategory();
            $subcategories->category_id = $category_id;
            $subcategories->subcategory_name = $request->subcategory_name[$i];
            $subcategories->save();
        }
        Alert::success('Success Title', 'Subcategory added successfully');
        return redirect()->route('subcategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Subcategory $subcategory)
    {

        $categories = Category::all();
        return view('admin.subcategories.edit', compact('subcategory','categories'));
    }

    /**
     * @param Request $request
     * @param Subcategory $subcategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Subcategory $subcategory)
    {
        $request->validate([
            'subcategory_name'=>'required'
        ]);
        $category_id = $request->input('category_id');
        $subcategory_name = $request->input('subcategory_name');

        $subcategory->category_id = $category_id;
        $subcategory->subcategory_name = $subcategory_name;
        $subcategory->update();
        Alert::success('Success Title', 'Subcategory updated successfully');
        return redirect()->route('subcategory.index');

    }

    /**
     * @param Subcategory $subcategory
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function destroy(Subcategory $subcategory)
    {
        $subcategory->delete();
        return redirect()->route('subcategory.index');
    }
}
