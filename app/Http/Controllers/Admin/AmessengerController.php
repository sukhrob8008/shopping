<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Amessenger;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AmessengerController extends Controller
{

    public function create()
    {

//        $org_sql = DB::select('select t1.architec_id, t2.name from amessengers as t1 inner join users as t2 on t1.architec_id=t2.id group by t1.architec_id, t2.name order by t1.architec_id desc');
//        dd($org_sql);
        $members = User::where('role','!=',1)->get();
        return view('admin.messengers.index', compact('members'));
    }

    public function chat_member($member)
    {
        $account = User::where('id','=',$member)->first();
        $old_messeges = Amessenger::where('architec_id','=',$member)->get();
        return view('admin.messengers.chat', compact('account', 'old_messeges'));
    }

    public function chat_with_admin()
    {
        $old_messeges_with_admin = Amessenger::where('architec_id','=',Auth::user()->id)->get();
        return view('architector.messengers.chat', compact('old_messeges_with_admin'));
    }


    public function user_chat_with_admin()
    {
        $old_messeges_with_admin = Amessenger::where('architec_id','=',Auth::user()->id)->get();
        return view('user.messenger.chat', compact('old_messeges_with_admin'));
    }
    /**
     * @param Request $request
     * @param $account
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function admin_message_text(Request $request, $account)
    {
        $request->validate([
            'admin_messege'=>'required'
        ]);

        $amessege = new Amessenger();
        $amessege->admin_id = Auth::user()->id;
        $amessege->architec_id = $account;
        $amessege->admin_messege = $request->input('admin_messege');
        $amessege->architec_messege = null;
        $amessege->save();

        return redirect()->route('members.chat', $account);
    }

    public function architec_text_message(Request $request)
    {
        $request->validate([
            'architec_messege'=>'required'
        ]);
        $amessege = new Amessenger();
        $amessege->admin_id = 1;
        $amessege->architec_id = Auth::user()->id;
        $amessege->architec_messege = $request->input('architec_messege');
        $amessege->admin_messege = null;
        $amessege->save();

        return redirect()->route('admin.chat');

    }

    public function user_text_message(Request $request)
    {
        $request->validate([
            'user_messege'=>'required'
        ]);
        $amessege = new Amessenger();
        $amessege->admin_id = 1;
        $amessege->architec_id = Auth::user()->id;
        $amessege->architec_messege = $request->input('user_messege');
        $amessege->admin_messege = null;
        $amessege->save();

        return redirect()->route('user.chat');

    }

    public function search(Request $request)
    {
        $result = DB::table('users')
            ->where('name','like','%'.$request->search.'%')
            ->where('role','!=',1)
            ->get();
        return response()->json([
            'status'=>true,
            'result'=>$result
        ]);
    }

}
