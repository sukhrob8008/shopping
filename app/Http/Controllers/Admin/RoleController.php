<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
//        $roles = DB::select('')
        return view('admin.role.view', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
//        $datetime_1 = '2022-04-10 11:15:30';
//        $datetime_2 = '2022-04-12 13:30:45';
//
//        $start_datetime = new DateTime($datetime_1);
//        $diff = $start_datetime->diff(new DateTime($datetime_2));
//        dd($diff);
        $users = User::all();
//        dd($users);
        return view('admin.role.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required',
            'role_id'=>'required'
        ]);
        $user_id = $request->input('user_id');
        $role_id = $request->input('role_id');
        User::where('id','=',$user_id)->update([
            'role'=>$role_id
        ]);
//        DB::select('insert into roles(user_id, role_id) values(?,?)',[$user_id, $role_id]);
        $role = new Role();
        $role->user_id = $user_id;
        $role->role_id = $role_id;
        $role->save();
        Alert::success('Success Title', 'Role added successfully');
        return redirect()->route('role.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Role $role)
    {

        $users = User::select('id','name','lname','role')->where('id','=',$role->user_id)->get();
        $user_id = $users[0]->id;
        $user_name = $users[0]->name;
        $user_lname = $users[0]->lname;
        $user_role = $users[0]->role;
        return view('admin.role.edit', compact('role','user_role','user_id','user_name','user_lname'));
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Role $role)
    {

        $role_id = $request->input('role_id');
        User::where('id','=',$role->user_id)
            ->update([
                'role'=>$role_id
            ]);
        $role->role_id = $role_id;
        $role->update();
        Alert::success('Success Title', 'Role updated successfully');
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
