<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Petition;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PetitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $petitions = Petition::where('user_id','=',Auth::user()->id)->orderBy('id','desc')->get();
        return view('user.petitions.view', compact('petitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.petitions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'petition_text'=>'required'
        ]);
        $petition_text = $request->input('petition_text');
        $portfolio = $request->input('portfolio_link');
        $petition = new Petition();
        $petition->user_id = Auth::user()->id;
        $petition->status = 0;
        $petition->petition_text = $petition_text;
        $petition->portfolio_link = $portfolio;
        $petition->save();
        Alert::success('Success Title', 'Petition sent successfully');
        return redirect()->route('petition.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Petition $petition)
    {
        return view('user.petitions.edit', compact('petition'));
    }

    /**
     * @param Request $request
     * @param Petition $petition
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Petition $petition)
    {
        $petition_text = $request->input('petition_text');
        $portfolio = $request->input('portfolio_link');

        $petition->petition_text = $petition_text;
        $petition->portfolio_link = $portfolio;
        $petition->update();
        Alert::success('Success Title', 'Petition updated successfully');
        return redirect()->route('petition.index');
    }

    /**
     * @param Petition $petition
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Petition $petition)
    {
        $petition->delete();
        return redirect()->route('petition.index');
    }

    public function notifications()
    {
        $petitions = Petition::where('status','=',0)->orderBy('id','desc')->get();
        return view('admin.petitions.index', compact('petitions'));
    }

    public function notificationshow(Petition $petition)
    {
        return view('admin.petitions.show', compact('petition'));
    }

    public function notification_accept(Petition $petition)
    {

        Petition::where('id','=',$petition->id)->update([
            'status'=>1
        ]);
        User::where('id','=',$petition->user_id)->update([
            'role'=>2
        ]);

        return redirect()->route('dashboard.notifications');
    }

    public function notification_cancel(Request $request, Petition $petition)
    {
        $result = $request->input('result_description');
        Petition::where('id','=', $petition->id)
            ->update([
                'status'=>2,
                'result_description'=>$result
            ]);
        return redirect()->route('dashboard.notifications');
    }
}
