<?php

namespace App\Http\Controllers\Architector;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class SpamController extends Controller
{
    public function architec_spam(Request $request, $architector)
    {
        User::where('id','=',$architector)
            ->update([
                'spam_status'=>1,
                'spam_reason'=>$request->spam_reason
            ]);
        return redirect()->route('dashboard.architectors');
    }

    public function architec_nospam($architector)
    {
        User::where('id','=',$architector)
            ->update([
                'spam_status'=>0,
                'spam_reason'=>'No spam'
            ]);
        return redirect()->route('dashboard.architectors');
    }

    public function user_spam(Request $request, $user)
    {
        User::where('id','=',$user)
            ->update([
                'spam_status'=>1,
                'spam_reason'=>$request->spam_reason
            ]);
        return redirect()->route('dashboard.users');
    }

    public function user_nospam($user)
    {
        User::where('id','=',$user)
            ->update([
                'spam_status'=>0,
                'spam_reason'=>'No spam'
            ]);
        return redirect()->route('dashboard.users');
    }

}
