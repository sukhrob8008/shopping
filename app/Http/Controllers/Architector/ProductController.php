<?php

namespace App\Http\Controllers\Architector;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image as InterventionImage;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('user_id','=', Auth::user()->id)->orderBy('id','desc')->get();
        return view('architector.products.view', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('architector.products.create', compact('categories', 'subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'model_name'=>'required',
            'category_id'=>'required',
            'subcategory_id'=>'required',
            'model_price'=>'required',
            'model_img' => 'required|array',
            'model_img.*' => 'mimes:jpeg,png,jpg,svg',
            'model_file'=>'required',
            'model_description'=>'required',
        ]);

        $org_images = '';
        $big_images = '';
        $medium_images = '';
        $small_images = '';

        $imageSizes = config('images.sizes');
        $model_name = $request->input('model_name');
        $category_id = $request->input('category_id');
        $subcategory_id = $request->input('subcategory_id');
        $model_price = $request->input('model_price');
        $model_description = $request->input('model_description');
        $model_size = $request->file('model_file')->getSize();

        for ($i=0; $i<count($request->file('model_img')); $i++)
        {
            $all_img = $request->file('model_img')[$i];

            $extension = $all_img->getClientOriginalExtension();
            $fileNameToStore = time();

            $org_images .= $fileNameToStore.$i.'.'.$extension.',';


            $big = $fileNameToStore.$i.'_'.$imageSizes['big']['width'].'x'.
                $imageSizes['big']['height'].'.'.$extension;

            $medium = $fileNameToStore.$i.'_'.$imageSizes['medium']['width'].'x'.
                $imageSizes['medium']['height'].'.'.$extension;

            $small = $fileNameToStore.$i.'_'.$imageSizes['small']['width'].'x'.
                $imageSizes['small']['height'].'.'.$extension;

            $big_images .= $big.',';
            $medium_images .= $medium.',';
            $small_images .= $small.',';
            InterventionImage::make($all_img)
                ->fit($imageSizes['big']['width'], $imageSizes['big']['height'])
                ->save(public_path('architec/models/images/big/').$big);

            InterventionImage::make($all_img)
                ->fit($imageSizes['medium']['width'], $imageSizes['medium']['height'])
                ->save(public_path('architec/models/images/medium/').$medium);

            InterventionImage::make($all_img)
                ->fit($imageSizes['small']['width'], $imageSizes['small']['height'])
                ->save(public_path('architec/models/images/small/').$small);

            $all_img->move(public_path('architec/models/images/orginals/'), $fileNameToStore.$i.'.'.$extension);

        }

        $model_file = $request->file('model_file');
        $extension_file = $model_file->getClientOriginalExtension();
        $orginal_name = time().'.'.$extension_file;
        if ($extension_file == 'max')
        {
            $model_file->move(public_path('architec/models/files/max/'), $orginal_name);
        }elseif ($extension_file == 'blend')
        {
            $model_file->move(public_path('architec/models/files/blend/'),$orginal_name);
        }elseif ($extension_file == 'obj')
        {
            $model_file->move(public_path('architec/models/files/obj/'),$orginal_name);
        }elseif ($extension_file == 'fbx')
        {
            $model_file->move(public_path('architec/models/files/fbx/'),$orginal_name);
        }

        $products = new Product();
        $products->user_id = Auth::user()->id;
        $products->category_id = $category_id;
        $products->subcategory_id = $subcategory_id;
        $products->model_name = $model_name;
        $products->model_price = $model_price;
        $products->model_img_org = $org_images;
        $products->model_img_big = $big_images;
        $products->model_img_medium = $medium_images;
        $products->model_img_small = $small_images;
        $products->model_file = $orginal_name;
        $products->model_description = $model_description;
        $products->model_size = $model_size;
        $products->model_type = $extension_file;
        $products->status_reason = "Your 3D model is checking now!";
        $products->save();
        Alert::success('Success Title', 'Product added successfully');
        return redirect()->route('product.index');
    }

    /**
     * @param Product $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function show(Product $product)
    {
        $model_big_images = explode(',', $product->model_img_big);
        $model_medium_images = explode(',', $product->model_img_medium);
        $model_small_images = explode(',', $product->model_img_small);
//        dd($model_big_images[0]);
        return view('architector.products.show', compact('product','model_big_images','model_medium_images','model_small_images'));
    }


    public function edit(Product $product)
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $model_small_images = explode(',', $product->model_img_small);
        return view('architector.products.edit',compact('product','categories','subcategories','model_small_images'));
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'model_name'=>'required',
            'category_id'=>'required',
            'subcategory_id'=>'required',
            'model_price'=>'required',
            'model_img.*' => 'mimes:jpeg,png,jpg,svg',
            'model_file' => 'mimes:max,fbx,obj,blend',
            'model_description'=>'required',
        ]);
        $imageSizes = config('images.sizes');
        $model_name = $request->input('model_name');
        $category_id = $request->input('category_id');
        $subcategory_id = $request->input('subcategory_id');
        $model_price = $request->input('model_price');
        $model_description = $request->input('model_description');


        if ($request->hasFile('model_img'))
        {
            $old_images_big = explode(',', $product->model_img_big);
            $old_images_medium = explode(',', $product->model_img_medium);
            $old_images_small = explode(',', $product->model_img_small);
            $old_images_org = explode(',', $product->model_img_org);
            for ($i=0; $i<count($old_images_big)-1; $i++)
            {
                \Illuminate\Support\Facades\File::delete(public_path('architec/models/images/big/'.$old_images_big[$i]));
                \Illuminate\Support\Facades\File::delete(public_path('architec/models/images/medium/'.$old_images_medium[$i]));
                \Illuminate\Support\Facades\File::delete(public_path('architec/models/images/small/'.$old_images_small[$i]));
                \Illuminate\Support\Facades\File::delete(public_path('architec/models/images/orginals/'.$old_images_org[$i]));
            }

            $big_images = '';
            $medium_images = '';
            $small_images = '';
            $org_images = '';
            for ($i=0; $i<count($request->file('model_img')); $i++)
            {
                $all_img = $request->file('model_img')[$i];

                $extension = $all_img->getClientOriginalExtension();
                $fileNameToStore = time();

                $org_images .= $fileNameToStore.$i.'.'.$extension.',';

                $big = $fileNameToStore.$i.'_'.$imageSizes['big']['width'].'x'.
                    $imageSizes['big']['height'].'.'.$extension;

                $medium = $fileNameToStore.$i.'_'.$imageSizes['medium']['width'].'x'.
                    $imageSizes['medium']['height'].'.'.$extension;

                $small = $fileNameToStore.$i.'_'.$imageSizes['small']['width'].'x'.
                    $imageSizes['small']['height'].'.'.$extension;

                $big_images .= $big.',';
                $medium_images .= $medium.',';
                $small_images .= $small.',';
                InterventionImage::make($all_img)
                    ->fit($imageSizes['big']['width'], $imageSizes['big']['height'])
                    ->save(public_path('architec/models/images/big/').$big);

                InterventionImage::make($all_img)
                    ->fit($imageSizes['medium']['width'], $imageSizes['medium']['height'])
                    ->save(public_path('architec/models/images/medium/').$medium);

                InterventionImage::make($all_img)
                    ->fit($imageSizes['small']['width'], $imageSizes['small']['height'])
                    ->save(public_path('architec/models/images/small/').$small);


                $all_img->move(public_path('architec/models/images/orginals/'), $fileNameToStore.$i.'.'.$extension);

            }
        }else{
            $big_images = $product->model_img_big;
            $medium_images = $product->model_img_medium;
            $small_images = $product->model_img_small;
            $org_images = $product->model_img_org;
        }

        if ($request->hasFile('model_file'))
        {
            \Illuminate\Support\Facades\File::delete(public_path('architec/models/files/'.$product->model_type.'/'.$product->model_file));
            $model_size = $request->file('model_file')->getSize();
            $model_file = $request->file('model_file');
            $extension_file = $model_file->getClientOriginalExtension();
            $orginal_name = time().'.'.$extension_file;
            if ($extension_file == 'max')
            {
                $model_file->move(public_path('architec/models/files/max/'), $orginal_name);
            }elseif ($extension_file == 'blend')
            {
                $model_file->move(public_path('architec/models/files/blend/'),$orginal_name);
            }elseif ($extension_file == 'obj')
            {
                $model_file->move(public_path('architec/models/files/obj/'),$orginal_name);
            }elseif ($extension_file == 'fbx')
            {
                $model_file->move(public_path('architec/models/files/fbx/'),$orginal_name);
            }
        }else{
            $model_size = $product->model_size;
            $orginal_name = $product->model_file;
            $extension_file = $product->model_type;
        }

        $product->user_id = Auth::user()->id;
        $product->category_id = $category_id;
        $product->subcategory_id = $subcategory_id;
        $product->model_name = $model_name;
        $product->model_price = $model_price;
        $product->model_img_org = $org_images;
        $product->model_img_big = $big_images;
        $product->model_img_medium = $medium_images;
        $product->model_img_small = $small_images;
        $product->model_file = $orginal_name;
        $product->model_description = $model_description;
        $product->model_size = $model_size;
        $product->model_type = $extension_file;
        $product->update();
        Alert::success('Success Title', 'Product updated successfully');
        return redirect()->route('product.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function select_category(Request $request)
    {
        return response()->json([
            'status'=>true,
            'subcategories'=>Subcategory::where('category_id','=',$request->category_id)->get()
        ]);
    }

    public function select_subcategory(Request $request)
    {
        return response()->json([
            'status'=>true,
            'categories'=>Category::all(),
            'category_id'=>Subcategory::select('category_id')->where('id','=',$request->subcategory_id)->first()
        ]);
    }

    public function recieve()
    {
//        $products = Product::all();
        $products = Product::where('status','=',0)->get();
        return view('admin.models.recieve', compact('products'));
    }

    public function show_receive(Product $product)
    {
        $model_big_images = explode(',', $product->model_img_big);
        $model_medium_images = explode(',', $product->model_img_medium);
        $model_small_images = explode(',', $product->model_img_small);
        return view('admin.models.show', compact('product','model_big_images','model_medium_images','model_small_images'));
    }

    public function accept_models()
    {
        $products = Product::where('status','=',1)->get();
        return view('admin.models.accept', compact('products'));
    }
    public function cancel_models()
    {
        $products = Product::where('status','=',2)->get();
        return view('admin.models.cancel', compact('products'));
    }

    public function press_accept(Product $product)
    {
        Product::where('id','=',$product->id)
            ->update([
                'status'=>1,
                'block'=>1,
                'status_reason'=>'Congratulations'
            ]);
        Alert::success('Success Title', 'This product join to website successfully');
        return redirect()->route('product.recieve');
    }

    public function reason(Request $request, Product $product)
    {
        Product::where('id','=',$product->id)
            ->update([
                'status'=>2,
                'status_reason'=>$request->status_reason
            ]);
        return redirect()->route('product.recieve');
    }


    public function accept_block(Request $request)
    {
        Product::where('id','=',$request->product_id)
            ->update([
                'block'=>$request->status
            ]);
        return response()->json([
            'status'=>true
        ]);
    }

    public function product_detail(Product $product_accept)
    {
        $medium_images_array = explode(',', $product_accept->model_img_medium);
        $big_images_array = explode(',', $product_accept->model_img_big);
        return view('main.product_details.index', compact('product_accept','medium_images_array', 'big_images_array'));
    }


}
