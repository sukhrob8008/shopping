<?php

namespace App\Providers;

use App\Views\PetitionComposer;
use Illuminate\Support\ServiceProvider;

class PetitionProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.layouts.master', PetitionComposer::class);
    }
}
