<?php

namespace App\Providers;

use App\Views\SubCategoryComposer;
use Illuminate\Support\ServiceProvider;

class SubCategoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('main.layouts.master', SubCategoryComposer::class);
    }
}
