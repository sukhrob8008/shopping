<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->string('model_name');
            $table->string('model_price');
            $table->string('model_img_big');
            $table->string('model_img_medium');
            $table->string('model_img_small');
            $table->string('model_file');
            $table->text('model_description');
            $table->string('model_size');
            $table->string('model_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
